<?php

/*
 * Get receiver Newsletter from Database and set in MS CRM
 * 
 * author Sami Jarmoud | treaction ag | sami.jarmoud@trection.de
 */

require_once('connection.php');

$response = $mailingWebservice->getMailingsBySchedulingTime($dateTimeNewsletter, false);
if ($response->isSuccess()) {
    foreach ($response->getResult() as $mailing) {
        // add toStringID in \src\DeliverySystems\Maileon\includes\com\maileon\api\mailings\Mailing.php
        $mailingArray[] = (string) $mailing->toStringID();
    }
}

$db = mysqli_connect($systemDbHost, $systemDbUser, $systemDbPassword, $systemDbName);
if (!$db) {
    exit("Verbindungsfehler: " . mysqli_connect_error());
}
//create Newsletter 

try {
    if (isset($mailingArray) && count($mailingArray) > 0) {
        foreach ($mailingArray as $mailingId) {
            $lesen = "select * from db_crnewsletter where isDone = 0 and mailing_id = '$mailingId' ";
            $auslesen = mysqli_query($db, $lesen);

            while ($row = mysqli_fetch_array($auslesen)) {
                $mailing_id = $row['mailing_id'];
                $contactid = $row['contactid'];
                $contactEmail = $row['contactEmail'];
                $campaignName = utf8_encode($row['campaignName']);
                $timestamp = $row['timestamp'];
                $guid = $row['guid'];

                $filterGuid = array(
                    'data' => array(
                        'attribute' => 'contactid',
                        'operator' => 'eq',
                        'value' => $guid
                    )
                );
                $contactGuidCount = $service->retrieveMultipleEntities('contact', true, $filterGuid, true, null, null, null, true, false);
                if ((int) $contactGuidCount->Count > 0) {
                    $createNewsletter = $service->entity('kkit_newsletter');
                    $createNewsletter->description = "Kampagnen ID: " . $mailing_id . " \n Name der Kampagne: " . $campaignName;
                    $createNewsletter->subject = $campaignName;
                    $createNewsletter->kkit_versanddatum = strtotime($timestamp);
                    $createNewsletter->regardingobjectid = new AlexaCRM\CRMToolkit\Entity\EntityReference('contact', $guid);
                    #$createNewsletter->from = new EntityReference('contact', $guid);
                    #$createNewsletter->to = new EntityReference('contact', $guid);
                    $newsletterid = $createNewsletter->create();

                    if (!empty($newsletterid)) {
                        $newsletterField = 'newsletter_guid' . $mailing_id;
                        $result = $contactWebservice->createCustomField($newsletterField, 'string');
                        $xqUpdateContact = new com_maileon_api_contacts_Contact();
                        $xqUpdateContact->anonymous = FALSE;
                        $xqUpdateContact->id = $mailing_id;
                        $xqUpdateContact->email = $contactEmail;
                        $xqUpdateContact->custom_fields[$newsletterField] = $newsletterid;
                        $responseCreate = $contactWebservice->createContact($xqUpdateContact, com_maileon_api_contacts_SynchronizationMode::$UPDATE, "src", "subscriptionPage", false, false);
                        if ($responseCreate->isSuccess()) {
                            $processUpdate = "UPDATE db_crnewsletter SET isDone = 1 , newsletter_id = '$newsletterid' WHERE contactid = '$contactid' and mailing_id = '$mailing_id' ";
                            $update = mysqli_query($db, $processUpdate);
                        }
                    }
                }
                //Kontakten, die nicht mehr in MS CRM gibt, werden sich in XQ abgemledet 
                else{
                    $unsubcriberMaileon = $contactWebservice->unsubscribeContactByEmail($contactEmail);
                }
            }
           
       }
    }
} catch (Exception $exc) {
    echo $exc->getTraceAsString();
}
