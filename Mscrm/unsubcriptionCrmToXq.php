<?php

require_once('connection.php');

$filterDate = array(
    'data' => array(
        'attribute' => 'modifiedon',
        'operator' => 'ge',
        'value' => $dateTimeUnsubCrm
    )
);
// CRM unsubscriptions
$contactEntity = $service->retrieveMultipleEntities((string) $xmlConfig->ListOfObjects->contact->Name, true, $filterDate, true, null, null, null, true, false);
$contactCount = $contactEntity->Count;

//CRM unsubscriptions
try {
    if ((int) $contactCount > 0) {
        foreach ($contactEntity->Entities as $contact) {

            if (array_key_exists('emailaddress1', $contact)) {
                $email = $contact['emailaddress1'];
                $massenmails = (array)$contact['donotbulkemail'];
                if ($massenmails['Value']) {
                    //Get Contact By Email
                    $xqContactId = $contactWebservice->getContactByEmail($email);
                    if ($xqContactId->isSuccess()) {
                        $unsubcriberMaileonId = $xqContactId->getResult()->id;
                        $unsubcriberMaileon = $contactWebservice->unsubscribeContactById((int) $unsubcriberMaileonId);
                    }
                }
            }
        }
    }
} catch (Exception $exc) {
    echo $exc->getTraceAsString();
}


