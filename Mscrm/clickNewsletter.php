<?php

/*
 * Get Click Newsletter from XQ and set in MS CRM
 * 
 * author Sami Jarmoud | treaction ag | sami.jarmoud@trection.de
 */

require_once('connection.php');

$response = $mailingWebservice->getMailingsBySchedulingTime($fieldDateTime, false);
if ($response->isSuccess()) {
    foreach ($response->getResult() as $mailing) {
        $mailingArray[] = (string) $mailing->toStringID();
    }
} 
//Clicks
try {
    if (isset($mailingArray) && count($mailingArray) > 0) {
        foreach ($mailingArray as $mailingId) {
            $newsletterGuid = 'newsletter_guid' . $mailingId;

            $reportclicksCount = $reportsWebservice->getUniqueClicksCount(null, null, array($mailingId));
            if ($reportclicksCount->isSuccess()) {
                $maileonContactClickCount = $reportclicksCount->getResult();
                $contactClickLoop = ($maileonContactClickCount / 1000);
                $contactClickLoopCount = (int) $contactClickLoop + 1;

                for ($countClcik = 1; $countClcik <= $contactClickLoopCount; $countClcik++) {
                    $reportclicks = $reportsWebservice->getClicks(null, null, array($mailingId), null, null, null, null, null, null, null, null, null, false, false, null, array($newsletterGuid), false, $countClcik, 1000);
                    if ($reportclicks->isSuccess()) {
                        
                        foreach ($reportclicks->getResultXML() as $click) {
                            foreach ($click->contact as $contactClick) {
                                if (count($contactClick->custom_fields) > 0) {
                                    $newsletter_guid_click = (string) $contactClick->custom_fields->field->value;
                                    if (!empty($newsletter_guid_click)) {
                                         $newsleterEntityClick = $service->entity('kkit_newsletter', $newsletter_guid_click);
                                          $newsleterEntityClick->kkit_klick = 1;
                                          $newsleterEntityClick->update();
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
} catch (Exception $exc) {
    echo $exc->getTraceAsString();
}