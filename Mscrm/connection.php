<?php

$rootPath = __DIR__ . \DIRECTORY_SEPARATOR;

\define('DIR_Crm_src', $rootPath . 'src' . \DIRECTORY_SEPARATOR);
\define('DIR_Crm_Entity', DIR_Crm_src . 'Entity' . \DIRECTORY_SEPARATOR);
\define('DIR_DeliverySystems', DIR_Crm_src . 'DeliverySystems' . \DIRECTORY_SEPARATOR);
\define('DIR_Packages', DIR_DeliverySystems . 'Packages' . \DIRECTORY_SEPARATOR);
\define('DIR_Api', DIR_Packages . 'Api' . \DIRECTORY_SEPARATOR);
\define('DIR_DeliverySystem', DIR_Api . 'DeliverySystem' . \DIRECTORY_SEPARATOR);
\define('DIR_Factory', DIR_DeliverySystem . 'Factory' . \DIRECTORY_SEPARATOR);

require_once('init.php');
require_once(DIR_Crm_src . 'Client.php');
require_once(DIR_Crm_Entity . 'EntityReference.php');
require_once(DIR_Crm_src . 'Settings.php');
require_once(DIR_Factory . 'Maileon.php');


require_once($rootPath . 'conf.php');
#$xmlConfigFile = 'conf.xml';
#if(file_exists($xmlConfigFile)){
#$xmlConfig = simplexml_load_file($xmlConfigFile);
#}
$xmlConfig = new SimpleXMLElement($xmlConfigFile);

$settings = array(
    'serverUrl' => $xmlConfig->Credentials->MSCRM->serverUrl,
    'username' => $xmlConfig->Credentials->MSCRM->username,
    'password' => $xmlConfig->Credentials->MSCRM->password,
    'authMode' => (string) $xmlConfig->Credentials->MSCRM->authMode,
);
$authentification = array(
    "BASE_URI" => $xmlConfig->Credentials->xq->BASE_URI,
    "API_KEY" => $xmlConfig->Credentials->xq->API_KEY,
    "PROXY_HOST" => "",
    "PROXY_PORT" => "",
    "THROW_EXCEPTION" => $xmlConfig->Credentials->xq->THROW_EXCEPTION,
    "TIMEOUT" => $xmlConfig->Credentials->xq->TIMEOUT, // 5 seconds
    "DEBUG" => $xmlConfig->Credentials->xq->DEBUG // NEVER enable on production
);


// Newsletter
$dateNowNewsletter = new DateTime('now');
$dateNowNewsletter->sub(new DateInterval('P1D'));
$dateNowNewsletter->setTime(00, 00, 00);
$dateTimeNewsletter = $dateNowNewsletter->format('Y-m-d H:i:s');

$dateNowImport = new DateTime('now');
$dateNowImport->setTime(00, 00, 00);
$dateTimeImport = $dateNowImport->format('Y-m-d H:i:s');

//Report
$dateNowReportField = new DateTime('now');
$dateNowReportField->sub(new DateInterval('P3D'));
$dateNowReportField->setTime(00, 00, 00);
$fieldDateTime = $dateNowReportField->format('Y-m-d H:i:s');

//unsubscription
$dateNowUnsubXq = new DateTime('now');
$dateNowUnsubXq->sub(new DateInterval('P1D'));
$dateTimeUnsubXq = $dateNowUnsubXq->format('d-m-Y');

$dateNowUnsubCrm = new DateTime('now');
$dateNowUnsubCrm->sub(new DateInterval('PT13H'));
$dateTimeUnsubCrm = $dateNowUnsubCrm->format('Y-m-d H:i:s');

//contact
$dateNow = new DateTime('now');
$dateNow->sub(new DateInterval('PT180M'));
$dateTime = $dateNow->format('Y-m-d H:i:s'); //'2016-11-11 15:00:00'

//CRM Settings 
$serviceSettings = new \AlexaCRM\CRMToolkit\Settings($settings);
$service = new \AlexaCRM\CRMToolkit\Client($serviceSettings);

//Maileon Settings
$contactWebservice = AlexaCRM\CRMToolkit\DeliverySystems\Packages\Api\DeliverySystem\Factory\Maileon::getWebservice(
                'ContactsService', $authentification
);
$contactWebservice->setDebug(FALSE);

$reportsWebservice = AlexaCRM\CRMToolkit\DeliverySystems\Packages\Api\DeliverySystem\Factory\Maileon::getWebservice(
                'ReportsWebservice', $authentification
);
$reportsWebservice->setDebug(FALSE);

$mailingWebservice = AlexaCRM\CRMToolkit\DeliverySystems\Packages\Api\DeliverySystem\Factory\Maileon::getWebservice(
                'MailingsWebservice', $authentification
);
$mailingWebservice->setDebug(FALSE);


$systemDbHost = $xmlConfig->Database->systemDbHost;
$systemDbName = $xmlConfig->Database->systemDbName;
$systemDbNameMarketingList = $xmlConfig->Database->systemDbNameMarketingList;
$systemDbNameNewsletter = $xmlConfig->Database->systemDbNameNewsletter;
$systemDbUser = $xmlConfig->Database->systemDbUser;
$systemDbPassword = $xmlConfig->Database->systemDbPassword;
