<?php
/*
 * Synchronization of contacts from MS CRM to XQ
 * 
 * author Sami Jarmoud | treaction ag | sami.jarmoud@trection.de
 */

require_once('connection.php');

$filterDate = array(
    'data' => array(
        'attribute' => 'modifiedon',
        'operator' => 'ge',
        'value' => $dateTime
    )
);

//Contact Entity
$contactEntity = $service->retrieveMultipleEntities((string) $xmlConfig->ListOfObjects->contact->Name, true, $filterDate, true, null, null, null, true, false);
$contactCount = $contactEntity->Count;
//Contact

try {
    if ((int) $contactCount > 0) {
        foreach ($contactEntity->Entities as $contact) {
            if(array_key_exists('emailaddress1', $contact)){
            foreach ($xmlConfig->ListOfObjects->contact->Fields->Field as $filed) {
                 if(array_key_exists((string) $filed->CRMName, $contact)){
                if (!is_null($contact[(string) $filed->CRMName])) {
                    if (!is_object($contact[(string) $filed->CRMName])) {                 
                        ${$filed->XQName} = utf8_encode($contact[(string) $filed->CRMName]);           
                    } 
                    else {
                        ${$filed->XQName} = '';            
                    }
                } else {
                    ${$filed->XQName} = '';
                }
                if (($filed->XQName == 'BIRTHDAY') &&
                        array_key_exists('birthdate', $contact)
                ) {
                    if (!is_null($contact[(string) $filed->CRMName])) {
                        ${$filed->XQName} = $contact[(string) $filed->CRMName];
                    } else {
                        ${$filed->XQName} = '';
                    }
                }
             }else{
                 ${$filed->XQName} = '';
             }
            }
            
             if (array_key_exists('parentcustomerid', $contact)) {
               if (!is_null($contact[(string) $xmlConfig->ListOfMappings->MappingContactAccount->SourceField])) { 
                   
                   $accountId = $contact[(string) $xmlConfig->ListOfMappings->MappingContactAccount->SourceField]->Value->__get('ID');   
               }
            } else {
                $accountId = '';
                $parentcustomerid ='';
           }
           
            $mappingAccountEntity = $service->entity((string) $xmlConfig->ListOfObjects->account->Name, $accountId);
            foreach ($xmlConfig->ListOfObjects->account->Fields->Field as $filed) {
                if (!is_null($mappingAccountEntity->propertyValues[(string) $filed->CRMName]['Value'])) {
                    if (is_object($mappingAccountEntity->propertyValues[(string) $filed->CRMName]['Value'])) {
                        ${$filed->XQName} = $mappingAccountEntity->propertyValues[(string) $filed->CRMName]['Value'];
                        if (!is_null(${$filed->XQName})) {
                            ${$filed->XQName} = ${$filed->XQName}->__get('label');
                        } else {
                            ${$filed->XQName} = '';
                        }
                    }else{
                         ${$filed->XQName} = $mappingAccountEntity->propertyValues[(string) $filed->CRMName]['Value'];
                    }
                } else {
                    ${$filed->XQName} = '';
                }
            }
            
            //Create Contact in Maileon
            $massenmails = (array)$contact['donotbulkemail'];
            $massenmailsBoolean = ($massenmails['Value'] == '1') ? 0 : 1;
            $marketinmaterial = (array)$contact['donotsendmm'];
            $marketinmaterialBoolean = ($marketinmaterial == '1') ? 0 : 1;

            if (strlen($GENDER) > 0) {
                $GENDERMappen = ($GENDER == '1') ? 'm' : 'w';
           } else {
                $GENDERMappen = '';
            }

            if(!empty($BIRTHDAY)){
                $BirthdayArray = (array) $BIRTHDAY;
                $BirthdayString = $BirthdayArray['FormattedValue'];
            $year = substr($BirthdayString, -2);
             if ($year > '15') {
                $BIRTHDAYMappen = substr_replace($BirthdayString, '.19', 5, 1);
               } else {
                $BIRTHDAYMappen = substr_replace($BirthdayString, '.20', 5, 1);
              }
            }else{
                $BIRTHDAYMappen ='';
            }
            // massenmails soll true sein
            if ($massenmailsBoolean) {
                $xqContact = new com_maileon_api_contacts_Contact();
                $xqContact->anonymous = FALSE;
                 $createdonArray = (array)$contact['createdon'];
                 $modifiedonArray = (array)$contact['modifiedon'];
                if ($createdonArray['Value'] != $modifiedonArray['Value']) {
                    //update
                    $responceId = $contactWebservice->getContactByEmail($email);
                    if ($responceId->isSuccess()) {
                        $contactMaileonId = $responceId->getResult()->id;
                        $xqContact->id = (int) $contactMaileonId;
                    }                 
                }
                $xqContact->email = $email;
                $xqContact->permission = com_maileon_api_contacts_Permission::${$xmlConfig->Credentials->xq->Permission};
                $xqContact->standard_fields[com_maileon_api_contacts_StandardContactField::$FIRSTNAME] = $FIRSTNAME;
                $xqContact->standard_fields[com_maileon_api_contacts_StandardContactField::$LASTNAME] = $LASTNAME;
                $xqContact->standard_fields[com_maileon_api_contacts_StandardContactField::$GENDER] = $GENDERMappen;
                $xqContact->standard_fields[com_maileon_api_contacts_StandardContactField::$ZIP] = $ZIP;
                $xqContact->standard_fields[com_maileon_api_contacts_StandardContactField::$SALUTATION] = $SALUTATION;
                $xqContact->standard_fields[com_maileon_api_contacts_StandardContactField::$COUNTRY] = $COUNTRY;
                $xqContact->standard_fields[com_maileon_api_contacts_StandardContactField::$ADDRESS] = $ADDRESS;
                $xqContact->standard_fields[com_maileon_api_contacts_StandardContactField::$CITY] = $CITY;
                #$xqContact->standard_fields[com_maileon_api_contacts_StandardContactField::$REGION] = $REGION;
                #$xqContact->standard_fields[com_maileon_api_contacts_StandardContactField::$STATE] = $STATE;
                $xqContact->standard_fields[com_maileon_api_contacts_StandardContactField::$BIRTHDAY] = $BIRTHDAYMappen;
                $xqContact->custom_fields["Position"] = $Position;
                $xqContact->custom_fields["Telefon (geschaeftlich)"] = $Telefon;
                
                $xqContact->custom_fields["Kontakt"] = $Kontakt;
                $xqContact->custom_fields["Firmenname"] = $Firmenname;
                $xqContact->custom_fields["massenmails"] = $massenmailsBoolean;
                $xqContact->custom_fields["marketinmaterial"] = $marketinmaterialBoolean;
                $xqContact->custom_fields["Kategorie"] = $kategorie;
                $xqContact->custom_fields["Unterkategorie"] = $unterkategorie;
                $xqContact->custom_fields["Investorentyp"] = $investorentyp;
                $xqContact->custom_fields["Branche"] = $branche;
                $xqContact->custom_fields["Subbranche"] = $subbranche;
                
               $responseUpdate = $contactWebservice->createContact($xqContact, com_maileon_api_contacts_SynchronizationMode::$UPDATE, "src", "subscriptionPage", false, false);
            }
        }
     }
    }
} catch (Exception $exc) {
    echo $exc->getTraceAsString();
}
