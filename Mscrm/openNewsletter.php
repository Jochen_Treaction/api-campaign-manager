<?php

/*
 * Get Opener Newsletter from XQ and set in MS CRM
 * 
 * author Sami Jarmoud | treaction ag | sami.jarmoud@trection.de
 */
require_once('connection.php');

$response = $mailingWebservice->getMailingsBySchedulingTime($fieldDateTime, false);
if ($response->isSuccess()) {
    foreach ($response->getResult() as $mailing) {
        $mailingArray[] = (string) $mailing->toStringID();
    }
}

//Opener
try {
    if (isset($mailingArray) && count($mailingArray) > 0) {
        foreach ($mailingArray as $mailingId) {
            $newsletterGuid = 'newsletter_guid' . $mailingId;
            
            $reportOpensCount = $reportsWebservice->getUniqueOpensCount(null, null, array($mailingId));       
            if ($reportOpensCount->isSuccess()) {
                $maileonContactOpenCount = $reportOpensCount->getResult();        
                $contactOpenLoop = ($maileonContactOpenCount / 1000);
                $contactOpenLoopCount = (int) $contactOpenLoop + 1;

                for ($countOpen = 1; $countOpen <= $contactOpenLoopCount; $countOpen++) {
                    $reportOpens = $reportsWebservice->getUniqueOpens(null, null, array($mailingId), null, null, null, false, true, null, array($newsletterGuid), false, $countOpen, 1000);
                    if ($reportOpens->isSuccess()) {
                        
                        foreach ($reportOpens->getResultXML() as $opener) {                          
                            foreach ($opener->contact as $contactOpener) {
                                if (count($contactOpener->custom_fields) > 0) {
                                    $newsletter_guid_opener = (string) $contactOpener->custom_fields->field->value;
                                    if (!empty($newsletter_guid_opener)) {                                    
                                          $newsleterEntityOpener = $service->entity('kkit_newsletter', $newsletter_guid_opener);
                                          $newsleterEntityOpener->kkit_oeffnung = 1;
                                          $newsleterEntityOpener->update(); 
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
} catch (Exception $exc) {
    echo $exc->getTraceAsString();
}