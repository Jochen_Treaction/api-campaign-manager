<?php

/*
 * Get receiver Newsletter from XQ and set in Database
 * 
 * delete Newsletter Filed after X date (here X = 3)
 * author Sami Jarmoud | treaction ag | sami.jarmoud@trection.de
 */

require_once('connection.php');

$response = $mailingWebservice->getMailingsBySchedulingTime($dateTimeImport, false);
if ($response->isSuccess()) {
    foreach ($response->getResult() as $mailing) {
        $mailingArray[] = (string) $mailing->toStringID();
    }
}
$db = mysqli_connect($systemDbHost, $systemDbUser, $systemDbPassword, $systemDbName);

//import Newsletter 
if (!$db) {
    exit("Verbindungsfehler: " . mysqli_connect_error());
}

             

                                
try {
    if (isset($mailingArray) && count($mailingArray) > 0) {
        foreach ($mailingArray as $mailingId) {
            $responseConatctCount = $reportsWebservice->getRecipientsCount(null, null, array($mailingId));
           
            if ($responseConatctCount->isSuccess()) {
                $maileonContactCount = $responseConatctCount->getResult();
                $contactLoop = ($maileonContactCount / 1000);
                $contactLoopCount = $contactLoop + 1;
                for ($count = 1; $count <= $contactLoopCount; $count++) {
                    $responseConatct = $reportsWebservice->getRecipients(null, null, array($mailingId), null, null, null, false, null, array("Kontakt"), false, $count, 1000);

                    if ($responseConatct->isSuccess()) {
                        foreach ($responseConatct->getResultXML() as $subscriber) {
                            foreach ($subscriber->contact as $xqContact) {
                                $contactid = (string) $xqContact->id;
                                $contactEmail = (string) $xqContact->email;
                                if (count($xqContact->custom_fields) > 0) {
                                    $guid = (string) $xqContact->custom_fields->field->value;
                                } else {
                                    $guid = '';
                                }
                            }
                            $mailing_id = (string) $subscriber->mailing_id;
                            $timestamp = (string) $subscriber->timestamp;
                            $campaignName = $mailingWebservice->getName($mailing_id);
                            $campaignNameResult = utf8_decode((string) $campaignName->getResult());

                            if (!empty($guid)) {
                                $eintrag = "INSERT INTO db_crnewsletter (guid , contactid, contactEmail, mailing_id, timestamp, campaignName) VALUES('$guid' , '$contactid' , '$contactEmail' , '$mailing_id' , '$timestamp' , '$campaignNameResult')";
                                $eintragen = mysqli_query($db, $eintrag);
                            }
                        }
                    }
                }
            }
        }       
    }

    
     //Feld loeschen nach 3 Tage von der Versanddatum
        $responceMailings = $mailingWebservice->getMailingsBySchedulingTime($fieldDateTime);
        if ($responceMailings->isSuccess()) {
            foreach ($responceMailings->getResultXML() as $mailingId) {
                $filedName = 'newsletter_guid' . $mailingId->id;
                $contactWebservice->deleteCustomField($filedName);
            }
        }
} catch (Exception $exc) {
    echo $exc->getTraceAsString();
}
