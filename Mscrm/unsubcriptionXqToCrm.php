<?php

require_once('connection.php');

//XQ unsubscriptions
try {

    $fromDate = strtotime($dateTimeUnsubXq).'000';
    $toDate = null;
    $mailingIds = null;
    $contactIds = null;
    $contactEmails = null;
    $data = array();
    #for ($count = 1; $count < 5; $count++) {
        $responceUnsub = $reportsWebservice->getUnsubscribers(
                $fromDate, $toDate, $mailingIds, $contactIds, $contactEmails, null, null, false, 1, 1000
        );
        if ($responceUnsub->isSuccess()) {
            foreach ($responceUnsub->getResultXML() as $unsubcriber) {
                $data[] = array(
                    'email' => (string) $unsubcriber->contact->email,
                    'id' => (int) $unsubcriber->contact->id
                );
            }
        }
    #}

  
    if (count($data) > 0) {
        foreach ($data as $unsubEmail) {
            $filterMail = array(
                'data' => array(
                    'attribute' => 'emailaddress1',
                    'operator' => 'eq',
                    'value' => $unsubEmail['email']
                )
            );
            $unsubcriberContact = $service->retrieveMultipleEntities((string) $xmlConfig->CampaignResults->Unsubscribe->Object, true, $filterMail, true, null, null, null, true, false);

            if ($unsubcriberContact->Count > 0) {
                $contact = $unsubcriberContact->Entities[0];
                if (array_key_exists('emailaddress1', $contact)) {
                    $contactId = $unsubcriberContact->Entities[0][(string) $xmlConfig->CampaignResults->Unsubscribe->PrimaryKeyField];
                    $unsubcriberContactEntity = $service->entity((string) $xmlConfig->CampaignResults->Unsubscribe->Object, $contactId);
                    $fieldName = (string) $xmlConfig->CampaignResults->Unsubscribe->Field;
                    $unsubcriberContactEntity->$fieldName = 1;
                    $unsubcriberContactEntity->update();
                }
            }
        }
    }
     
    
} catch (Exception $ex) {
    echo $ex->getTraceAsString();
}


