<?php
$xmlConfigFile = <<<XML
<?xml version="1.0" encoding="utf-8"?>
<MSCRMAdapter>
    <Credentials>
        <!-- BASE-URI ist die URL von Maileon, API-KEY Secret fĂĽr den Zugriff, Proxy optional falls Proxy eingeschaltet wurde-->
        <!-- TROW Exception nur fĂĽr Debugging bei Live Betrieb aus, analog Debug = "false"-->
        <xq>
            <BASE_URI>https://api.maileon.com/1.0</BASE_URI>
            <API_KEY>433e24f6-d924-4b2b-a4b5-09cf8205a71b</API_KEY>
            <PROXY_HOST></PROXY_HOST>
            <PROXY_PORT></PROXY_PORT>
            <THROW_EXCEPTION>true</THROW_EXCEPTION>
            <TIMEOUT>100000</TIMEOUT>
            <DEBUG>false</DEBUG>
            <!-- Default Permission NONE - Keine, SOI-Single Opt-In, COI-Confirmed Opt-In, DOI-Double Opt-In, DOI_PLUS Double Opt-In Einzelnutzer, OTHER - anderes Verfahren -->
            <Permission>DOI_PLUS</Permission>
        </xq>
        <MSCRM>
            <serverUrl>https://catcapgmbh.crm4.dynamics.com</serverUrl>
            <username>TreactionAPI@CatCapGmbH.onmicrosoft.com</username>
            <password>Zoja7016</password>
            <authMode>OnlineFederation</authMode>
            <!-- 1. Option "static" es wird immer das gleiche in XQ gesetzt fĂĽr die jeweils aktuelle Marketingliste -->
            <!-- Das Feld im Standard lautet CRMSelection vom Typ Boolean -->
            <!-- Bei der Ăśbernahme einer Marketingliste wird fĂĽr alle Kontakte das CRMSelection=false gesetzt -->
            <!-- im zweiten Schritt werden dann die gewĂ¤hlten Kontakte auf true gesetzt-->
            <!-- 2. Option "dynamic" wie bei 1. nur das fĂĽr jede Marketinglist ein neues Feld in XQ angelegt wird -->
            <!-- das Feld bekommt automatisch den Namen der Marketingliste -->
            <!-- Vorteil bei Option 2 es kĂ¶nnen mehrere Selektionen gefahren zur gleichen Zeit gefahren werden-->
            <!--Falls der Kunde mit Static und Dynamic Marketingliste types benutzt und falls nur Dynamic Marketingliste benutzt, dann static auf false setzen -->
            <!-- Default static und dynamic auf true setzen -->
            <marketinglistfield>static</marketinglistfield>
        </MSCRM>
    </Credentials>
    <!--- Intervall in Sekunden -->
    <CronJob>
        <!-- CronJobs synchronisieren die Systeme alle 2 Minuten-->
         <Intervall>120</Intervall>
        <!--FĂĽr die Ăśbernahme der Ă–ffner und Klicks in MS CRM muss temporĂ¤r die GUID der AktivitĂ¤t gespeichet werden. Nach dieser Zeitspanne in Sekunden wird die Referenz gelĂ¶scht Standard sind 3 Tage-->
         <SyncBack>259.200</SyncBack>
    </CronJob>
    <Database>
        <systemDbHost>localhost</systemDbHost>   
        <systemDbName>dbmarketigliste</systemDbName>
		<systemDbNameMarketingList>marketingListeID</systemDbNameMarketingList>
        <systemDbNameNewsletter>db_crnewsletter</systemDbNameNewsletter>
        <systemDbUser>db_marketing</systemDbUser>
        <systemDbPassword>6P3b9D7b</systemDbPassword>
    </Database>
    <ListOfObjects>
    
        <!-- Wenn die API NULL oder LEER "" liefert wird der Default Wert genommen-->
        <!-- Name ist der fachliche Name des Felders, XQName ist der technische Name in XQ, Field ist der technische Name im MS CRM -->
        <contact>
            <Name>contact</Name>
            <PrimaryKeyField>contactid</PrimaryKeyField>
            <MonitorChanges>true</MonitorChanges>
            <Fields>
                <Field>
                    <XQName>Kontakt</XQName>
                    <CRMName>contactid</CRMName>
                    <Type>string</Type>
                </Field>
                <Field>
                    <XQName>email</XQName>
                    <CRMName>emailaddress1</CRMName>
                    <Type>string</Type>
                </Field>
                <Field>
                    <XQName>SALUTATION</XQName>
                    <CRMName>salutation</CRMName>
                    <Type>string</Type>
                </Field>
                <Field>
                    <XQName>FIRSTNAME</XQName>
                    <CRMName>firstname</CRMName>
                    <Type>string</Type>
                </Field>
                <Field>
                    <XQName>LASTNAME</XQName>
                    <CRMName>lastname</CRMName>
                    <Type>string</Type>
                </Field>
               <Field>
                    <XQName>GENDER</XQName>
                    <CRMName>gendercode</CRMName>
                    <Type>string</Type>
                </Field>
                <Field>
                    <XQName>ADDRESS</XQName>
                    <CRMName>address1_line1</CRMName>
                    <Type>string</Type>
                </Field>
                <Field>
                    <XQName>ZIP</XQName>
                    <CRMName>address1_postalcode</CRMName>
                    <Type>string</Type>
                </Field>
                <Field>
                    <XQName>CITY</XQName>
                    <CRMName>address1_city</CRMName>
                    <Type>string</Type>
                </Field>
                
                <Field>
                    <XQName>COUNTRY</XQName>
                    <CRMName>address1_country</CRMName>
                    <Type>string</Type>
                </Field>
                <Field>
                    <XQName>massenmails</XQName>
                    <CRMName>donotbulkemail</CRMName>
                    <Default>true</Default>
                    <Type>booelan</Type>
                </Field>
                <Field>
                    <XQName>marketinmaterial</XQName>
                    <CRMName>donotsendmm</CRMName>
                    <Type>booelan</Type>
                    <Default>true</Default>
                </Field>
                <Field>
                    <XQName>BIRTHDAY</XQName>
                    <CRMName>birthdate</CRMName>
                    <Type>date</Type>
                </Field>
                <Field>
                    <XQName>Telefon</XQName>
                    <CRMName>telephone1</CRMName>
                    <Type>string</Type>
                </Field>
                <Field>
                    <XQName>Position</XQName>
                    <CRMName>jobtitle</CRMName>
                    <Type>string</Type>
                </Field>
                <Field>
                    <XQName>createdon</XQName>
                    <CRMName>createdon</CRMName>
                    <Type>string</Type>
                </Field>
                <Field>
                    <XQName>modifiedon</XQName>
                    <CRMName>modifiedon</CRMName>
                    <Type>string</Type>
                </Field>
            </Fields>
        </contact>
        <account>
            <Name>account</Name>
            <PrimaryKeyField>accountid</PrimaryKeyField>
            <MonitorChanges>true</MonitorChanges>
            <Fields>
                <Field>
                    <XQName>Firmenname</XQName>
                    <CRMName>name</CRMName>
                    <Type>string</Type>
                </Field>
                <Field>
                    <XQName>kategorie</XQName>
                    <CRMName>accountcategorycode</CRMName>
                    <Type>string</Type>
                </Field>
                <Field>
                    <XQName>unterkategorie</XQName>
                    <CRMName>kkit_unterkategorie</CRMName>
                    <Type>string</Type>
                </Field>
                <Field>
                    <XQName>investorentyp</XQName>
                    <CRMName>kkit_investorentyp</CRMName>
                    <Type>string</Type>
                </Field>
                <Field>
                    <XQName>branche</XQName>
                    <CRMName>industrycode</CRMName>
                    <Type>string</Type>
                </Field>
                <Field>
                    <XQName>subbranche</XQName>
                    <CRMName>catcap_subbranche</CRMName>
                    <Type>string</Type>
                </Field>
            </Fields>
        </account>
        <subscription>
            <Name>kkit_subscription</Name>
            <PrimaryKeyField>kkit_subscriptionid</PrimaryKeyField>
            <MonitorChanges>false</MonitorChanges>
            <Fields>
                <Field>
                    <XQName>kkit_name</XQName>
                    <CRMName>kkit_name</CRMName>
                    <Type></Type>
                </Field>
                <Field>
                    <XQName>kkit_subscriptionid</XQName>
                    <CRMName>kkit_subscriptionid</CRMName>
                    <Type></Type>
                </Field>
                <Field>
                    <XQName>createdon</XQName>
                    <CRMName>createdon</CRMName>
                    <Type></Type>
                </Field>
                <Field>
                    <XQName>modifiedon</XQName>
                    <CRMName>modifiedon</CRMName>
                    <Type></Type>
                </Field>
            </Fields>
        </subscription>
        <marketiglist>
            <Name>list</Name>
            <PrimaryKeyField>listid</PrimaryKeyField>
            <Fields>
                <Field>listname</Field>
                <Field>query</Field>
                <Field>type</Field>
            </Fields>
        </marketiglist>
    </ListOfObjects>
    <ListOfMappings>
        <!--Beispiel Mapping ĂĽber 1:N Beziehung mit Primary und Foreign Key auf den EntititĂ¤ten-->
        <!-- Source Object ist der EntityName der Quelle, TargetObject ist der EntityName des Ziels-->
        <!-- Source Field ist der Name desFeldes auf dem Quell-Object, TargetField ist das GegenstĂĽck auf dem Zielobject-->
        <MappingContactAccount>
            <SourceObject>contact</SourceObject>
            <TargetObject>account</TargetObject>
            <SourceField>parentcustomerid</SourceField>
            <TargetField>accountid</TargetField>
        </MappingContactAccount>
        <!-- Spezialfall n:n mit einem Beziehungsobjekt-->
        <MappingMappingContactSubscription>
            <SourceObject>contact</SourceObject>
            <TargetObject>kkit_subscription</TargetObject>
            <Intersection>kkit_contact_kkit_subscription</Intersection>
        </MappingMappingContactSubscription>
        <MappingAccountContact>
            <SourceObject>account</SourceObject>
            <TargetObject>contact</TargetObject>
            <SourceField>accountid</SourceField>
            <TargetField>parentcustomerid</TargetField>
        </MappingAccountContact>
    </ListOfMappings>
    <CampaignResults>
        <!--Abmelder werden mit dem MS CRM synchronisiert. Die Informationen werden in diese Felder geschrieben-->
        <!--Optionales Feld ist Date, nur bei Sync=true sollen die Werte synchronisiert werden-->
        <Unsubscribe>
            <Sync>true</Sync>
            <Object>contact</Object>
            <PrimaryKeyField>contactid</PrimaryKeyField>
            <Field>donotbulkemail</Field>   
            <Date>UnsubscribeDateTime</Date>
        </Unsubscribe>
        <Action>
            <Object>kkit_newsletter</Object>
            <Subject>FieldSubject</Subject>
            <Body>FieldBody</Body>
            <From>FieldSender</From>
            <To>FieldRecipient</To>
            <Date>SentDate</Date>
            <Opened>Opened</Opened>
            <Clicked>Clicked</Clicked>
        </Action>
        <ListOfMappings>
            <Mapping>
                <Object>contact</Object>
                <XQName>Kontakt</XQName>
                <CRMName>contactid</CRMName>
            </Mapping>
        </ListOfMappings>
    </CampaignResults>
</MSCRMAdapter>

XML;
?>