<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace AlexaCRM\CRMToolkit\DeliverySystems\Packages\Api\DeliverySystem\Factory;

use AlexaCRM\CRMToolkit\DeliverySystems\Packages\Api\DeliverySystem\Factory\InterfaceWebserviceFactory;


\define('DIR_Maileon', DIR_DeliverySystems . 'Maileon' . \DIRECTORY_SEPARATOR);
\define('DIR_includes', DIR_Maileon . 'includes' . \DIRECTORY_SEPARATOR);
require_once(DIR_includes.'MaileonApiClient.php');
 
#require_once('src/DeliverySystems/Maileon/includes/MaileonApiClient.php');


/**
 * Description of Maileon
 *
 * @author SamiMohamedJarmoud
 */
class Maileon implements InterfaceWebserviceFactory{
    /**
	 * getWebservice
	 * 
	 * @param string $webserviceType
         * @param mixed $auth 
	 * @return 
	 * @throws \DomainException
	 */
        public static function getWebservice($webserviceType, $auth) {
		try {
			switch ($webserviceType) {
				case 'MailingsWebservice':
					$webservice = new \com_maileon_api_mailings_MailingsService($auth); 
					break;
                                case 'ReportsWebservice':
                                        $webservice = new \com_maileon_api_reports_ReportsService($auth);
                                        break;
                                case 'ContactsService':
                                        $webservice = new \com_maileon_api_contacts_ContactsService($auth);
                                        break;
				default:
					throw new \DomainException('Unknown webserviceObject: ' . $webserviceType);
			}

			return $webservice;
		} catch (\Exception $e) {
			// TODO: log message
			throw $e;
		}
	}
        
        /**
	 * getWebserviceObjectByType
	 * 
	 * @param string $webserviceType
	 * @return \SoapClient
	 * @throws \DomainException
	 */
       public static function getInstance($webserviceType) {
            return 0;
        }
}
