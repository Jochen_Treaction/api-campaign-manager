<?php
namespace AlexaCRM\CRMToolkit\DeliverySystems\Packages\Api\DeliverySystem\Factory;

/**
 * Description of InterfaceWebserviceFactory
 *
 * @author Cristian.Reus
 */
interface InterfaceWebserviceFactory {
	
	/**
	 * getInstance
	 * 
	 * @param string $webserviceType
	 * @return mixed
	 */
	public static function getInstance($webserviceType);
        
        /**
	 * getWebservice
	 * 
	 * @param string getWebservice
         * @param mixed $auth 
	 * @return mixed
	 */
	public static function getWebservice($webserviceType, $auth);
}
