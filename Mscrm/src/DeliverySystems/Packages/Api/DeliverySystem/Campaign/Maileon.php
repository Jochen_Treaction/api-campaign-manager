<?php

namespace Packages\Api\DeliverySystem\Campaign;

use Packages\Api\DeliverySystem\Factory\Maileon as MaileonFactory;
use Packages\Api\DeliverySystem\Campaign\AbstractCampaign;
use Packages\Api\DeliverySystem\Domain\Entity\MailingListEntity;
use Packages\Api\DeliverySystem\Domain\Entity\MailingEntity;
use Packages\Core\Persistence\ObjectStorage;
use Packages\Core\Utility\EncodingUtility;
use Packages\Api\DeliverySystem\Utility\DeliverySystemUtiltiy;

/**
 * Description of Sendeffect
 * 
 * @author Sami.Jamroud
 */
Class Maileon extends AbstractCampaign {

    /**
     * createMailingPropertiesMapping
     * 
     * @var array
     */
    protected $createMailingPropertiesMapping = array(
        'fromName' => 'SenderAlias',
         'fromEmail' => 'Sender'
    );

    /**
     * updateMailingPropertiesMapping
     * 
     * @var array
     */
    protected $updateMailingPropertiesMapping = array(
        'fromName' => 'SenderAlias',
        'subject' => 'Subject',
        'title' => 'Name',
        'fromEmail' => 'Sender'
    );

    /**
     * readMailingPropertiesMapping
     * 
     * @var array
     */
    protected $readMailingPropertiesMapping = array(
        'title' => 'Name',
        'subject' => 'Subject',
        'fromName' => 'FromName',
        'fromEmail' => 'FromEmailPrefix',
        'mimeType' => 'MimeType',
        'charset' => 'Charset',
        'createdDate' => 'CreatedDate',
        'maxRecipients' => 'MaxRecipients',
        'recipientFilterIds' => 'RecipientFilterIds',
        'recipientListIds' => 'RecipientListIds',
        'scheduleDate' => 'ScheduleDate',
        'status' => 'Status'
    );

    /**
     * getList
     * 
     * @param string $status
     * @param integer $limit
     * @param integer $offset
     * @return \Packages\Core\Persistence\ObjectStorage
     */
    public function getList($status, $limit = 20, $offset = 0) {
        
    }

    /**
     * create
     * 
     * @param MailingEntity $mailingEntity
     * @param \Modules\Campaign\Domain\Model\CampaignEntity $campaignEntity
     * @return integer campaignId
     * @throws \Exception
     */
    public function create(MailingEntity $mailingEntity, \Modules\Campaign\Domain\Model\CampaignEntity $campaignEntity) {

        try {
            $mailingWebservice = MaileonFactory::getWebservice(
                            'MailingsWebservice', $this->authentication->getAuthentification()
            );

            $debug = "false";
            $mailingWebservice->setDebug($debug);

            $mailingId = $this->createMailing(
                    $campaignEntity->getK_id(),
                    $campaignEntity->getBetreff(), 
                    $mailingWebservice
            );
            //Update Kampagne Name 
            $campaignName = $this->validationZeichen($campaignEntity->getK_name());
            $mailingWebservice->setName(
                    $mailingId,
                    $this->validationCampaignName($campaignEntity->getK_id() .'-'.$campaignName)
                    );
            //click Profile
             $mailingWebservice->setTags(
                    $mailingId, 
                    DeliverySystemUtiltiy::getShortcutClickProfiles($campaignEntity->getCampaign_click_profiles_id())
                    );
            try {

                $this->processUpdateMailingWebserviceData(
                        $this->createMailingPropertiesMapping, 
                        $mailingWebservice, 
                        $mailingEntity, 
                        $mailingEntity,
                        $campaignEntity, 
                        (int) $mailingId,
                        FALSE
                );
            } catch (Exception $ex) {
                $this->delete($mailingId);
                throw $ex;
            }


            #$mailingWebservice->setTargetGroupId(
            # $mailingId, 
            #$mailingEntity->getRecipientListIds()[0]
            #); // the ID of the target group

            return (int) $mailingId;

            // TODO: scheduleDate
            #$this->processScheduleMailing(
            #	$mailingWebservice,
            #	$mailingEntity
            #);
        } catch (\Exception $ex) {
            $this->delete($mailingId);
            throw $ex;
        }
    }

    /**
     * read
     * 
     * @param integer $mailingId
     * @param boolean $getContent
     * @return \Packages\Api\DeliverySystem\Domain\Entity\MailingEntity
     */
    public function read($mailingId, $getContent = false) {
        
    }

    /**
     * update
     * 
     * @param MailingEntity $mailingEntity
     * @param MailingEntity $newMailingEntity
     * @param \Modules\Campaign\Domain\Model\CampaignEntity $campaignEntity
     * @return boolean
     */
    public function update(MailingEntity $mailingEntity, MailingEntity $newMailingEntity, \Modules\Campaign\Domain\Model\CampaignEntity &$campaignEntity) {

        if ($mailingEntity->_isNew()) {
            throw new \InvalidArgumentException('The domainEntity "(' . \get_class($mailingEntity) . ')" is new.', 1446713194);
        }

        try {
            $mailingWebservice = MaileonFactory::getWebservice(
                            'MailingsWebservice', $this->authentication->getAuthentification()
            );

            $debug = "false";
            $mailingWebservice->setDebug($debug);
           
             //click Profile
             $mailingWebservice->setTags(
                    $mailingEntity->getUid(), 
                    DeliverySystemUtiltiy::getShortcutClickProfiles($campaignEntity->getCampaign_click_profiles_id())
                    );
                    
            $this->processUpdateMailingWebserviceData(
                    $this->updateMailingPropertiesMapping, 
                    $mailingWebservice, 
                    $mailingEntity, 
                    $newMailingEntity, 
                    $campaignEntity, 
                    $mailingEntity->getUid(), 
                    TRUE
            );

            //processScheduleMailing
            $this->processScheduleMailing(
            $mailingWebservice,
            $mailingEntity,
            $campaignEntity        
            );  

            $result = TRUE;
        } catch (\Exception $ex) {
            throw $ex;
        }
        return $result;
    }

    /**
     * delete
     * 
     * @param integer $mailingId
     * @return boolean
     * @throws \InvalidArgumentException
     * @throws Exception
     */
    public function delete($mailingId) {
        $result = FALSE;

        try {
            if ((int) $mailingId <= 0) {
                throw new \InvalidArgumentException('invalid mailingId', 1446713342);
            }

            $mailingWebservice = MaileonFactory::getWebservice(
                            'MailingsWebservice', $this->authentication->getAuthentification()
            );


            $debug = "false";
            $mailingWebservice->setDebug($debug);

            #$mailingStatus = $this->processGetFieldDataByMailingId(
            #$mailingWebservice,
            #(int) $mailingId,
            #$this->readMailingPropertiesMapping['status']
            #);
            #switch ($mailingStatus) {
            #case 'NEW':
            $response = $mailingWebservice->deleteMailing(
                    $mailingId
            );
            if (!$response->isSuccess()) {
                throw new \InvalidArgumentException($response->getBodyData());
            }
            $result = TRUE;
            #break;
            #}
        } catch (\Exception $e) {
            throw $e;
        }

        return $result;
    }

    /**
     * send
     * 
     * @param MailingEntity $MailingEntity
     * @param \Modules\Campaign\Domain\Model\CampaignEntity $campaignEntity
     * @param boolean $updateAutoNv
     * @return boolean
     */
    public function send(MailingEntity $mailingEntity, \Modules\Campaign\Domain\Model\CampaignEntity &$campaignEntity) {
        
    }

    /**
     * autoNvMailings
     * 
     * @param integer $oldMailingId mailingId
     * @return integer
     */
    public function autoNvMailings($oldMailingId, \Modules\Campaign\Domain\Model\CampaignEntity &$campaignEntity) {
        try {

            $mailingWebservice = MaileonFactory::getWebservice(
                            'MailingsWebservice', $this->authentication->getAuthentification()
            );


            $newMailingId = $this->copyMailing($oldMailingId);

            # if (!$response->isSuccess()) {
            #throw new \InvalidArgumentException($response->getBodyData());
            #}
            if (\is_numeric($newMailingId)) {
                /**
                 * clear targetGroups
                 */
                #$oldRecipientFilterIds = $mailingWebservice->getRecipientFilterIds(
                #$this->authentication->getAuthentification(),
                #$newMailingId
                #);
                #$recipientFilterIds = $this->processClearMailingRecipientFilterWebservices($oldRecipientFilterIds);
                #$mailingWebservice->setRecipientFilterIds(
                #$this->authentication->getAuthentification(),
                #$newMailingId,
                #$recipientFilterIds
                #);
                #unset($oldRecipientFilterIds, $recipientFilterIds);
                $mailingWebservice->setName(
                        $newMailingId, 
                        $this->validationCampaignName($campaignEntity->getK_id().'-'.  $this->validationZeichen($campaignEntity->getK_name()))
                );
                $mailingWebservice->setSubject(
                        $newMailingId, 
                        $campaignEntity->getBetreff()
                );
           
             //click Profile
             $mailingWebservice->setTags(
                    $newMailingId, 
                    DeliverySystemUtiltiy::getShortcutClickProfiles($campaignEntity->getCampaign_click_profiles_id())
                    );
            }
        } catch (\Exception $e) {
            var_dump('test');
            $this->delete($newMailingId);
            throw $e;
        }

        return (int) $newMailingId;
    }

    /**
     * resetShippingDate
     * 
     * @param integer $mailingId
     * @return integer
     */
    public function resetShippingDate($mailingId) {
        
        $this->deleteSchedule($mailingId);
        
    }

    /**
     * sendTestMail
     * @param integer $mailingId
     * @param long $recipientListId
     * @param array $recipient
     * @return integer
     */
    public function sendTestMail($mailingId, $recipientListId, array $recipient) {
        
         $mailingWebservice = MaileonFactory::getWebservice(
                            'MailingsWebservice', $this->authentication->getAuthentification()
            );
         
         try{
             $resultTestMail['senden'] = $mailingWebservice->sendTestmailToSingle(
                     $mailingId, 
                     $recipient['email']
                     );
   
         } catch (\Exception $ex) {
                throw $ex;
         }
         
        
    }

    protected function checkResult($result = "") {

        // What is retruned? can it be null?
        $statusCode = $result->getStatusCode();
        if (!$result->isSuccess()) {
            echo '<font color="#ff0000"><b>failed</b></font>' . ' (Status code: ' . $statusCode . ' - '
            . \com_maileon_api_HTTPResponseCodes::getStringFromHTTPStatusCode($statusCode)
            . ($result->getBodyData() ? ' - ' . $result->getBodyData() : '') . ')';
        } else {
            echo '<font color="#22ff22"><b>success</b></font>' . ' (Status code: ' . $statusCode . ' - '
            . \com_maileon_api_HTTPResponseCodes::getStringFromHTTPStatusCode($statusCode) . ')';
        }
    }

    /**
     * processUpdateMailingWebserviceData
     * 
     * @param array $processDataArray
     * @param  $mailingWebservice
     * @param  $mailingId
     * @param MailingEntity $mailingEntity
     * @param CampaignEntity $campaignEntity
     * @return void
     */
    protected function processUpdateMailingWebserviceData(array $processDataArray, $mailingWebservice, MailingEntity $mailingEntity, MailingEntity $newMailingEntity, \Modules\Campaign\Domain\Model\CampaignEntity &$campaignEntity, $mailingId, $updateInAsp) {
        foreach ($processDataArray as $key => $item) {
            if (
                    (
                    \is_array($mailingEntity->_getProperty($key)) && \count($mailingEntity->_getProperty($key)) > 0
                    ) || (
                    !\is_array($mailingEntity->_getProperty($key)) && \strlen($mailingEntity->_getProperty($key)) > 0
                    )
            ) {
                if ($mailingEntity->_isDirty($key)) {
                    $this->processSetFieldDataByMailingId(
                            $mailingWebservice, 
                            $mailingId, 
                            $item, 
                            $mailingEntity->_getProperty($key),
                            $campaignEntity
                    );
                }
            }
        }

        if ($mailingEntity->_isDirty('maxRecipients') && $mailingEntity->getMaxRecipients() > 0
        ) {
            
        }

        if (\count($mailingEntity->getRecipientListIds()) > 0) {
            $updateRecipientListIds = FALSE;

            $cleanRecipientListIds = $mailingEntity->_getCleanProperty('recipientListIds');
            if (!\is_null($cleanRecipientListIds)) {
                $comparisonResult = $mailingEntity->getRecipientListIds() === $cleanRecipientListIds;
                if (!$comparisonResult) {
                    $updateRecipientListIds = TRUE;
                }
            } else {
                $updateRecipientListIds = TRUE;
            }
            if ($updateRecipientListIds) {
                $this->processSetFieldDataByMailingId(
                        $mailingWebservice, 
                        $mailingId, 
                        'TargetGroupId', 
                        $mailingEntity->_getProperty('recipientListIds')[0],
                        $campaignEntity
                );
            }
        }

        if ($mailingEntity->_isDirty('htmlContent') && \strlen($mailingEntity->getHtmlContent()) > 0
        ) {

            $this->processSetOrUpdateContentAndTracking(
                    $mailingWebservice, (int) $mailingId, 'text/html', $mailingEntity->getHtmlContent()
            );
        }

        if ($mailingEntity->_isDirty('txtContent') && \strlen($mailingEntity->getTxtContent()) > 0
        ) {
            $this->processSetOrUpdateContentAndTracking(
                    $mailingWebservice, (int) $mailingId, 'text/plain', $mailingEntity->getTxtContent()
            );
        }
    }

    protected function processSetFieldDataByMailingId($mailingWebservice, $mailingId, $key, $value,  \Modules\Campaign\Domain\Model\CampaignEntity $campaignEntity) {
        if($key === 'Name'){
            $name = $this->validationCampaignName($campaignEntity->getK_id().'-'.$campaignEntity->getK_name());
            $value = $this->validationZeichen($name);
        }
        $mailingWebservice->{'set' . \ucfirst($key)}(
                $mailingId,
                $value
        );
    }

    /**
     * processSetOrUpdateContentAndTracking
     * 
     * @param $mailingWebservice
     * @param integer $mailingId
     * @param string $mimeType
     * @param string $content
     * @return void
     */
    protected function processSetOrUpdateContentAndTracking($mailingWebservice, $mailingId, $mimeType, $content) {
        switch ($this->charset) {
            case 'UTF-8':
                $content = EncodingUtility::encodeToUtf8($content);
                break;

            default:
                $content = EncodingUtility::encodeToIso($content);
                break;
        }
        switch ($mimeType) {
            case 'text/html':
                $mailingWebservice->setHTMLContent(
                        $mailingId, $content, true, true
                );
                break;
            case 'text/plain':
                $mailingWebservice->setTextContent(
                        $mailingId, $content
                );
                break;

            default :
                $mailingWebservice->setHTMLContent(
                        $mailingId, $content, true, true
                );
                $mailingWebservice->setTextContent(
                        $mailingId, $content
                );
        }
    }

    protected function createMailing($title, $subject, $mailingWebservice) {
        try {
            $response = $mailingWebservice->createMailing(
                    $title,
                    $subject, 
                    true, 
                    'regular'
            );

            if (!$response->isSuccess()) {
                throw new \InvalidArgumentException($response->getBodyData());
            }
           
            $mailingId = $response->getResult();
            
             // set Tracking
            $mailingWebservice->setTracking(
                    $mailingId,
                    'single-recipient'
                    );
        } catch (\Exception $ex) {
            throw $ex;
        }

        return $mailingId;
    }

    /**
     * copyMailing
     * 
     * @param integer $oldMailingId
     * @return integer
     * @throws \Packages\Api\DeliverySystem\Campaign\Exception
     */
    protected function copyMailing($oldMailingId) {
        try {
            $mailingWebservice = MaileonFactory::getWebservice(
                            'MailingsWebservice', $this->authentication->getAuthentification()
            );

            $debug = "false";
            $mailingWebservice->setDebug($debug);

            $response = $mailingWebservice->copyMailing(
                    $oldMailingId
            );
            if (!$response->isSuccess()) {
                throw new \InvalidArgumentException($response->getBodyData());
            }
            $newMailingId = $response->getResult();
        } catch (\Exception $e) {
            throw $e;
        }

        return (int) $newMailingId;
    }

    /**
     * processScheduleMailing
     * 
     * @param  $mailingWebservice
     * @param MailingEntity $mailingEntity
     * @return void
     */
    protected function processScheduleMailing($mailingWebservice, MailingEntity $mailingEntity, \Modules\Campaign\Domain\Model\CampaignEntity $campaignEntity) {
       $updateScheduleDate = $campaignEntity->_isDirty('datum');
      
        if ($mailingEntity->getScheduleDate() instanceof \DateTime
                && 
                $mailingEntity->getUseScheduleDate()
        ) {
              $this->processSetFieldDataByMailingId(
                        $mailingWebservice, 
                        $mailingEntity->getUid(), 
                        'TargetGroupId', 
                        $mailingEntity->getRecipientFilterIds(),
                        $campaignEntity
                );
            if ((boolean) $updateScheduleDate !== TRUE){
                $mailingWebservice->createSchedule(
                $mailingEntity->getUid(),
                $mailingEntity->getScheduleDate()->format('Y-m-d'), 
                $mailingEntity->getScheduleDate()->format('H'), 
                $mailingEntity->getScheduleDate()->format('i')
              );
            }else{
                $mailingWebservice->updateSchedule(
                $mailingEntity->getUid(),
                $mailingEntity->getScheduleDate()->format('Y-m-d'), 
                $mailingEntity->getScheduleDate()->format('H'), 
                $mailingEntity->getScheduleDate()->format('i')
              );               
            }
        }
    }
    
    /**
     * 
     * @param type $mailingId
     * @throws \Exception
     * @throws \InvalidArgumentException
     */
    protected function deleteSchedule($mailingId) {
        try{
                $mailingWebservice = MaileonFactory::getWebservice(
                            'MailingsWebservice', $this->authentication->getAuthentification()
            );

            $debug = "false";
            $mailingWebservice->setDebug($debug);
            
            $response = $mailingWebservice->deleteSchedule($mailingId);

             if (!$response->isSuccess()) {
                throw new \InvalidArgumentException($response->getBodyData());
            }
        } catch (\Exception $ex) {
            throw $ex;
        }
    }
    /**
     * 
     * @param string $name
     * @return string
     */
    protected function validationCampaignName($name) {
                $search = \str_replace('[', '(', $name);
                $title = \str_replace(']', ')', $search);
              return $title;  
    }
    /**
     * 
     * @param string $name
     * @return string
     */
    protected function validationZeichen($name){
        $input = \htmlspecialchars_decode($name);
          $zeichen = array(
              '&' => '-',
              '$' => '-',
              '%' => '-',
              '{' => '-',
              '}' => '-',
              '\\' => '-',
              '/' => '-',
              '`' => '-',
              '€' => '-',
              '*' => '-',
              '+' => '-',
              '~' => '-',
              '<' => '-',
              '>' => '-',
              ';' => '-',
              '|' => '-',
              '#' => '-' 
          );
          foreach ($zeichen as $key => $value){
              $ouput = \str_replace($key, $value, $input);
              $input = $ouput;
          }
          return $ouput;
    }
    
}
