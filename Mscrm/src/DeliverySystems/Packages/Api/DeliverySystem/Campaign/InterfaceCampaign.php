<?php
namespace Packages\Api\DeliverySystem\Campaign;

use Packages\Api\DeliverySystem\Domain\Entity\MailingEntity;


/**
 * Description of InterfaceCampaign
 *
 * @author Cristian.Reus
 */
interface InterfaceCampaign {
	
	/**
	 * getList
	 * 
	 * @param string $status
	 * @param integer $limit
	 * @param integer $offset
	 * @return \Packages\Core\Persistence\ObjectStorage
	 */
	public function getList($status, $limit = 20, $offset = 0);
	
	/**
	 * create
	 * 
	 * @param MailingEntity $mailingEntity
	 * @param \Modules\Campaign\Domain\Model\CampaignEntity $campaignEntity
	 * @return integer campaignId
	 */
	public function create(MailingEntity $mailingEntity, \Modules\Campaign\Domain\Model\CampaignEntity $campaignEntity);
	
	/**
	 * read
	 * 
	 * @param integer $mailingId
	 * @param boolean $getContent
	 * @return \Packages\Api\DeliverySystem\Domain\Entity\MailingEntity
	 */
	public function read($mailingId, $getContent = false);
	
	/**
	 * update
	 * 
	 * @param MailingEntity $mailingEntity
	 * @param MailingEntity $newMailingEntity
	 * @param \Modules\Campaign\Domain\Model\CampaignEntity $campaignEntity
	 * @return boolean
	 */
	public function update(MailingEntity $mailingEntity, MailingEntity $newMailingEntity, \Modules\Campaign\Domain\Model\CampaignEntity &$campaignEntity);
	
	/**
	 * delete
	 * 
	 * @param integer $mailingId mailingId
	 * @return mixed|boolean
	 */
	public function delete($mailingId);
	
	/**
	 * autoNvMailings
	 * 
	 * @param integer $oldMailingId mailingId
	 * @return integer
	 */
	public function autoNvMailings($oldMailingId, \Modules\Campaign\Domain\Model\CampaignEntity &$campaignEntity);
	
	/**
	 * resetShippingDate
	 * 
	 * @param integer $mailingId
	 * @return integer
	 */
	public function resetShippingDate($mailingId);
	
	/**
	 * sendTestMail
	 * @param integer $mailingId
	 * @param long $recipientListId
	 * @param array $recipient
	 * @return integer
	 */
	public function sendTestMail($mailingId, $recipientListId, array $recipient);
        
        /**
        * send
        * 
        * @param MailingEntity $MailingEntity
        * @param \Modules\Campaign\Domain\Model\CampaignEntity $campaignEntity
        * @return boolean
        */
    public function send(MailingEntity $mailingEntity, \Modules\Campaign\Domain\Model\CampaignEntity &$campaignEntity);
}
