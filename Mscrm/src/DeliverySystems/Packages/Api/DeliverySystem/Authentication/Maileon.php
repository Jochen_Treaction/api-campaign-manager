<?php
namespace Packages\Api\DeliverySystem\Authentication;

use Packages\Api\DeliverySystem\Authentication\AbstractAuthentication;

/**
 * Description of Sendeffect
 * 
 * @author Sami.Jarmoud
 */
class Maileon extends AbstractAuthentication{
    
 	/**
	 * login
	 * 
	 * @return void
	 */
	public function login(){
            if (\is_null($this->authentification)) {
                try {
                    $this->authentification = array(
                        "BASE_URI" => "https://api.maileon.com/1.0",
                        "API_KEY" => "08b90d60-d439-404f-bb93-4bade6e9582a",
                        "PROXY_HOST" => "",
                        "PROXY_PORT" => "",
                        "THROW_EXCEPTION" => true,
                        "TIMEOUT" => 100, // 5 seconds
                        "DEBUG" => "false" // NEVER enable on production
                    );
                } catch (Exception $ex) {
                    throw $ex;
                }
            }
        }
	
	/**
	 * logout
	 * 
	 * @return void
	 */
	public function logout(){
            $this->authentification = NULL;
        }  
}

