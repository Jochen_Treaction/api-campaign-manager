<?php

/**
 * getPackageNameFromByRequest
 * 
 * @param string $requestString
 * @return string
 */
function getPackageNameFromByRequest($requestString) {
	$dirName = \pathinfo($requestString, \PATHINFO_DIRNAME);
	$pathDataArray = \explode(\DIRECTORY_SEPARATOR, $dirName);
	
	return \end($pathDataArray);
}


$rootPath = __DIR__ . \DIRECTORY_SEPARATOR;

\define('DIR_Configs', $rootPath . 'Configs' . \DIRECTORY_SEPARATOR);
\define('DIR_Webservices', $rootPath . 'Webservices' . \DIRECTORY_SEPARATOR);
\define('DIR_Packages', $rootPath . 'Packages' . \DIRECTORY_SEPARATOR);


require_once($rootPath . 'Autoloader.php');