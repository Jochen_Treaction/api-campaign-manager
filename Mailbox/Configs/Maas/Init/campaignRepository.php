<?php
/* @var $clientEntity \Maas\Model\Entity\ClientEntity */

$dbClientHost = 'localhost';
$campaignTable = 'kampagnen';

if (!($clientEntity instanceof \Maas\Model\Entity\ClientEntity)) {
	throw new \InvalidArgumentException('no clientEntity!', 1441895623);
}

$clientConfigFile = \DIR_Configs . PACKAGES_NAME . \DIRECTORY_SEPARATOR . 'Connections' . \DIRECTORY_SEPARATOR . 'Clients' . \DIRECTORY_SEPARATOR . $clientEntity->getAbkz() . '.php';
if (\Maas\Utility\FileUtility::isReadable($clientConfigFile)) {
	require($clientConfigFile);
}

$campaignRepository = new \Maas\Model\Repository\CampaignRepository();
$campaignRepository->setDbName($dbClientName);
$campaignRepository->setDbUser($dbClientUser);
$campaignRepository->setDbPassword($dbClientPassword);
$campaignRepository->setHost($dbClientHost);
$campaignRepository->setTable($campaignTable);
$campaignRepository->init();