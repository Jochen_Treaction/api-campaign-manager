<?php
require_once(\DIR_Configs . PACKAGES_NAME . \DIRECTORY_SEPARATOR . 'Connections' . \DIRECTORY_SEPARATOR . 'systemConnection.php');

$clientRepository = new \Maas\Model\Repository\ClientRepository();
$clientRepository->setDbName($systemDbName);
$clientRepository->setDbUser($systemDbUser);
$clientRepository->setDbPassword($systemDbPassword);
$clientRepository->setHost($systemDbHost);
$clientRepository->init();