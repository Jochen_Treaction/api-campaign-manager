<?php
\header('Content-Type: application/json; charset=utf-8');
/**
 * wlog Write Log to file system in path/file:  /system/Module/Campaigns/Actions/log.txt
 * @param string              $hinweis
 * @param string|array|object $message
 * @param string              $file
 * @param int                 $line
 * @param bool                $with_script_tags
 * @author jsr
 * @since  2020-08-06
 * @usage  : e.g.: wlog('$qry',(!empty($qry)) ? $qry : 'NO $qry', __FILE__, __LINE__);// REMOVE
 */
if (!function_exists('wlog')) {
    function wlog($hinweis, $message, $file, $line, $OVERWRITE = false, $logToError = false)
    {
        if (false) { // COMMENT: LOG => TURN ON (true) AN OFF (false) HERE
            $logFile = ($logToError) ? '/errorLog_liveTicker.log' : '/log_liveTicker.log';
            try {
                $msg = "[" . date('Y-m-d H:i:s') . "] " . $hinweis . ' = ';
                $msg .= print_r($message, true);
                $msg .= "    [$file, $line]\n"; // cron/blCron/blacklistCron_2.php
                if ($OVERWRITE) {
                    file_put_contents(__DIR__ . $logFile, $msg);
                } else {
                    file_put_contents(__DIR__ . $logFile, $msg, FILE_APPEND);
                }
            } catch (Exception $e) {
                file_put_contents(__DIR__ . '/wlogError_liveTicker.log', $e->getMessage(), FILE_APPEND);
            }
        }
    }
}


require_once($_SERVER['DOCUMENT_ROOT'] . \DIRECTORY_SEPARATOR . 'config.php');

$package = 'Maas';

$dateTimeBegin = '2016-06-27 00:00:00';
$startCampaignCount = 3358;
$startLeadsCount = 361860;
$dateNow = new \DateTime();

$statusDataArray = array(
	'sql' => '(`status` > 19 OR `status` = 5)',
	'value' => '',
	'comparison' => ''
);


wlog('$_REQUEST', $_REQUEST, __FILE__, __LINE__); // REMOVE
$language = isset($_REQUEST['L']) ? \intval($_REQUEST['L']) : 0;
$languageMappingDataArray = array(
	0 => 'de',
	1 => 'en'
);

$resultDataArray = array();

try {
	if (\strlen($package) === 0) {
		throw new \RuntimeException('no valid package!', 1441888019);
	}
	\define('PACKAGES_NAME', $package);

	$autoloader = new \Autoloader();
	$autoloader->init();
	$autoloader->initErrorReporting();

	if (\Maas\Utility\CoreUtility::isProdSystem()) {
        wlog('isProdSystem', 'YES', __FILE__, __LINE__); // REMOVE
		$defaultFileLanguagePath = 'Language' . \DIRECTORY_SEPARATOR . 'Production' . \DIRECTORY_SEPARATOR . $languageMappingDataArray[0] . '.php';
        wlog('$defaultFileLanguagePath', $defaultFileLanguagePath, __FILE__, __LINE__); // REMOVE
	} else {
        wlog('isProdSystem', 'NO', __FILE__, __LINE__); // REMOVE
		$defaultFileLanguagePath = 'Language' . \DIRECTORY_SEPARATOR . 'Development' . \DIRECTORY_SEPARATOR . $languageMappingDataArray[0] . '.php';
        wlog('$defaultFileLanguagePath', $defaultFileLanguagePath, __FILE__, __LINE__); // REMOVE
	}

	require_once(\DIR_Configs . \PACKAGES_NAME . \DIRECTORY_SEPARATOR . 'Init' . \DIRECTORY_SEPARATOR . 'clientRepository.php');
    wlog('require_once', \DIR_Configs . \PACKAGES_NAME . \DIRECTORY_SEPARATOR . 'Init' . \DIRECTORY_SEPARATOR . 'clientRepository.php', __FILE__, __LINE__); // REMOVE

	/* @var $clientRepository \Maas\Model\Repository\ClientRepository */

	$dateBeginDataArray = array(
		'sql' => 'DATE_FORMAT(`datum`, \'%Y-%m-%d %T\')', // %Y-%m-%d %T  is something like '2020-08-06 10:14:25'
        'value' => $dateTimeBegin,
		'comparison' => \Maas\Factory\DbFactory::$fieldGreaterOrEqualThan
	);

	// nur versendete kampagnen
	$hvCampaign_queryPartsDataArray = array(
		'WHERE' => array(
			'campaignId' => array(
				'sql' => '`nv_id`',
				'value' => '`k_id`',
				'comparison' => 'fieldEqual'
			),
			'dateBegin' => $dateBeginDataArray,
			'status' => $statusDataArray
		)
	);

	$leadsQueryPartsDataArray = array(
		'SELECT' => 'SUM(`leads`) as `count`',
		'WHERE' => array(
			'dateBegin' => $dateBeginDataArray,
			'status' => $statusDataArray,
		)
	);
	$clicksQueryPartsDataArray = array(
		'SELECT' => 'SUM(`klicks_all`) as `count`',
		'WHERE' => array(
			'dateBegin' => $dateBeginDataArray,
			'status' => $statusDataArray,
		)
	);
	$clicksLeadsQueryPartsDataArray = array(
		'SELECT' => 'SUM(`klicks_all`) as `count`',
		'WHERE' => array(
			'dateBegin' => $dateBeginDataArray,
			'status' => $statusDataArray,
			'settlementType' => array(
				'sql' => '(`abrechnungsart` LIKE "CP%" OR `abrechnungsart` LIKE "HYB%")',
				'value' => '',
				'comparison' => ''
			),
		)
	);

	/**
	 * get all Clients
	 */
	$clientsDataArray = $clientRepository->findAll();


	foreach ($clientsDataArray as $clientEntity) {
		/* @var $clientEntity \Maas\Model\Entity\ClientEntity */


        wlog('======================================================= ', date('u'), '', ''); // REMOVE
        wlog('$clientEntity', $clientEntity->getId() . ' ' . $clientEntity->getMandant(), '', ''); // REMOVE

		/**
		 * TODO: später über status regeln
		 *
		 * id => 1, ist der testmandant
		 * nur hauptmandanten ausführen
		 */
		if ($clientEntity->getId() > 1
			&& $clientEntity->getParent_id() === 0
		) {
			require(\DIR_Configs . \PACKAGES_NAME . \DIRECTORY_SEPARATOR . 'Init' . \DIRECTORY_SEPARATOR . 'campaignRepository.php');
			/* @var $campaignRepository \Maas\Model\Repository\CampaignRepository */

			$resultDataArray[$clientEntity->getAbkz()]['campaigns'] = $campaignRepository->countResultByQueryParts($hvCampaign_queryPartsDataArray);
			$resultDataArray[$clientEntity->getAbkz()]['cpxLeads'] = $campaignRepository->getCampaignDataItemByQueryParts(
				$leadsQueryPartsDataArray,
				\PDO::FETCH_COLUMN
			);

			// wlog('$clientEntity->getAbkz()', $clientEntity->getAbkz(), __FILE__, __LINE__);
			wlog('[campaigns]', $resultDataArray[$clientEntity->getAbkz()]['campaigns'], '', '');
			wlog('[cpxLeads]', $resultDataArray[$clientEntity->getAbkz()]['cpxLeads'], '', '');

            $startCampaignCount += (!empty($resultDataArray[$clientEntity->getAbkz()]['campaigns'])) ? $resultDataArray[$clientEntity->getAbkz()]['campaigns'] : 0;
            $startLeadsCount += (!empty($resultDataArray[$clientEntity->getAbkz()][cpxLeads])) ? $resultDataArray[$clientEntity->getAbkz()][cpxLeads] : 0;


			$resultDataArray[$clientEntity->getAbkz()]['clicksLeadsCampaign'] = $campaignRepository->getCampaignDataItemByQueryParts(
				$clicksLeadsQueryPartsDataArray,
				\PDO::FETCH_COLUMN
			);
			$resultDataArray[$clientEntity->getAbkz()]['clicksAllCampaign'] = $campaignRepository->getCampaignDataItemByQueryParts(
				$clicksQueryPartsDataArray,
				\PDO::FETCH_COLUMN
			);

			if ($resultDataArray[$clientEntity->getAbkz()]['clicksLeadsCampaign'] > 0
				&& $resultDataArray[$clientEntity->getAbkz()]['cpxLeads'] > 0
			) {
				$resultDataArray[$clientEntity->getAbkz()]['clicksLeadsRatio'] = \round($resultDataArray[$clientEntity->getAbkz()]['clicksLeadsCampaign'] / $resultDataArray[$clientEntity->getAbkz()]['cpxLeads']);

				$resultDataArray[$clientEntity->getAbkz()]['additionalLeads'] = \round(($resultDataArray[$clientEntity->getAbkz()]['clicksAllCampaign'] - $resultDataArray[$clientEntity->getAbkz()]['clicksLeadsCampaign']) / $resultDataArray[$clientEntity->getAbkz()]['clicksLeadsRatio']);
			} else {
				$resultDataArray[$clientEntity->getAbkz()]['additionalLeads'] = 0;
			}

            $startLeadsCount += $resultDataArray[$clientEntity->getAbkz()]['additionalLeads'];

		}
	}

	try {
		if (isset($languageMappingDataArray[$language])) {
			if (\Maas\Utility\CoreUtility::isProdSystem()) {
				$fileLanguagePath = 'Language' . \DIRECTORY_SEPARATOR . 'Production' . \DIRECTORY_SEPARATOR . $languageMappingDataArray[$language] . '.php';
			} else {
				$fileLanguagePath = 'Language' . \DIRECTORY_SEPARATOR . 'Development' . \DIRECTORY_SEPARATOR . $languageMappingDataArray[$language] . '.php';
			}

			if (\Maas\Utility\FileUtility::isReadable($fileLanguagePath)) {
				require_once($fileLanguagePath);
			}
		} else {
			throw new \InvalidArgumentException('no language file defined for languageId: ' . $language);
		}
	} catch (\Exception $e) {
		\Maas\Utility\DebugAndExceptionUtility::sendDebugData(
			$e,
			'no language file defined for languageId: ' . $language
		);

		require_once($defaultFileLanguagePath);
	}
	/* @var $languageDataArray array */

	$jsonDataArray = array(
		$languageDataArray['campaigns'] => $startCampaignCount,
		$languageDataArray['leads'] => $startLeadsCount
	);
	foreach ($resultDataArray as $key => $items) {
		$jsonDataArray[$languageDataArray['campaigns']] += $items['campaigns'];
		$jsonDataArray[$languageDataArray['leads']] += $items['cpxLeads'] + $items['additionalLeads'];
	}
} catch (\Exception $e) {
	\Maas\Utility\DebugAndExceptionUtility::sendDebugData(
		$e,
		'Technisches Problem'
	);

	$jsonDataArray = array(
		'Kampagnen' => $startCampaignCount,
		'Leads' => $startLeadsCount
	);
}

if (isset($_GET['jsoncallback'])) {
	echo $_GET['jsoncallback'] . '(' . \json_encode($jsonDataArray) . ')';
} else {
	echo \json_encode($jsonDataArray);
}
