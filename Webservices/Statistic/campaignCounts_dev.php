<?php
\header('Content-Type: application/json; charset=utf-8');

require_once($_SERVER['DOCUMENT_ROOT'] . \DIRECTORY_SEPARATOR . 'config.php');

$package = 'Maas';

$dateTimeBegin = '2016-06-27 00:00:00';
$dateNow = new \DateTime();

$statusDataArray = array(
	'sql' => '(`status` > 19 OR `status` = 5)',
	'value' => '',
	'comparison' => ''
);

$resultDataArray = array();
$jsonDataArray = array(
	'campaigns' => 3356,
);

try {
	if (\strlen($package) === 0) {
		throw new \RuntimeException('no valid package!', 1464011938);
	}
	\define('PACKAGES_NAME', $package);
	
	$autoloader = new \Autoloader();
	$autoloader->init();
	$autoloader->initErrorReporting();
	
	require_once(\DIR_Configs . \PACKAGES_NAME . \DIRECTORY_SEPARATOR . 'Init' . \DIRECTORY_SEPARATOR . 'clientRepository.php');
	/* @var $clientRepository \Maas\Model\Repository\ClientRepository */
	
	$dateBeginDataArray = array(
		'sql' => 'DATE_FORMAT(`datum`, \'%Y-%m-%d %T\')',
		'value' => $dateTimeBegin,
		'comparison' => \Maas\Factory\DbFactory::$fieldGreaterOrEqualThan
	);
	
	// nur versendete kampagnen
	$hvCampaign_queryPartsDataArray = array(
		'WHERE' => array(
			'campaignId' => array(
				'sql' => '`nv_id`',
				'value' => '`k_id`',
				'comparison' => 'fieldEqual'
			),
			'dateBegin' => $dateBeginDataArray,
			'status' => $statusDataArray
		)
	);
	
	/**
	 * get all Clients
	 */
	$clientsDataArray = $clientRepository->findAll();
	foreach ($clientsDataArray as $clientEntity) {
		/* @var $clientEntity \Maas\Model\Entity\ClientEntity */
		
		/**
		 * TODO: später über status regeln
		 * 
		 * id => 1, ist der testmandant
		 * nur hauptmandanten ausführen
		 */
		if ($clientEntity->getId() > 1 
			&& $clientEntity->getParent_id() === 0
		) {
			require(\DIR_Configs . \PACKAGES_NAME . \DIRECTORY_SEPARATOR . 'Init' . \DIRECTORY_SEPARATOR . 'campaignRepository.php');
			/* @var $campaignRepository \Maas\Model\Repository\CampaignRepository */
			
			$resultDataArray[$clientEntity->getAbkz()]['campaigns'] = $campaignRepository->countResultByQueryParts($hvCampaign_queryPartsDataArray);
		}
	}
	
	foreach ($resultDataArray as $key => $items) {
		$jsonDataArray['campaigns'] += $items['campaigns'];
	}
} catch (\Exception $e) {
	\Maas\Utility\DebugAndExceptionUtility::sendDebugData(
		$e,
		'Technisches Problem'
	);
}

if (isset($_GET['jsoncallback'])) {
	echo $_GET['jsoncallback'] . '(' . \json_encode($jsonDataArray) . ')';
} else {
	echo \json_encode($jsonDataArray);
}