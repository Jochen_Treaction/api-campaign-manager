<?php
class Autoloader {
	
	/**
	 * init
	 * 
	 * @return	void
	 */
	public static function init() {
		try {
			\spl_autoload_register(
				array(
					__CLASS__,
					'loadPackagesClass'
				)
			);
		} catch (\Exception $e) {
			echo $e->getMessage();
		}
	}
	
	/**
	 * initErrorReporting
	 * 
	 * @return void
	 */
	public static function initErrorReporting() {
		\error_reporting(null);
		\ini_set('display_errors', false);
		
		if (\Maas\Utility\CoreUtility::getUserIp() == \Maas\Utility\CoreUtility::getDevIpAddress() 
			&& \Maas\Utility\CoreUtility::isDevSystem()
		) {
			\error_reporting(E_ALL);
			\ini_set('display_errors', true);
		}
	}
	
	
	
	
	
	/********************************************************************************************
	 *
	 *              load - functions
	 *
	 ****************************************************************************************** */
	/**
	 * loadPackagesClass
	 * 
	 * @param string $class
	 * return void
	 */
	protected static function loadPackagesClass($class) {
		require(\DIR_Packages . \PACKAGES_NAME . \DIRECTORY_SEPARATOR . 'directoryMapping.php');
		
		if (\count($packagesDirectoryMapping) > 0) {
			foreach ($packagesDirectoryMapping as $dir) {
				$correctClassName = \str_replace('\\', \DIRECTORY_SEPARATOR, $class);
				
				if ((\file_exists(\DIR_Packages . $correctClassName . '.php')) === true) {
					require_once(\DIR_Packages . $correctClassName . '.php');
				}
			}
		}
	}

}