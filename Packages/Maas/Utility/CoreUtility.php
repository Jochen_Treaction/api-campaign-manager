<?php
namespace Maas\Utility;

/**
 * CoreUtility
 */
final class CoreUtility {
	
	/**
	 * devIpAddress
	 * 
	 * @var string
	 */
	protected static $devIpAddress = '87.138.158.223';
	
	
	
	
	
	/**
	 * isProdSystem
	 * 
	 * @return boolean
	 */
	public static function isProdSystem() {
		return (self::getApiEnvironmentType() === 'Production');
	}
	
	/**
	 * isTestSystem
	 * 
	 * @return boolean
	 */
	public static function isTestSystem() {
		return (self::getApiEnvironmentType() === 'Testing');
	}
	
	/**
	 * isDevSystem
	 * 
	 * @return boolean
	 */
	public static function isDevSystem() {
		return (self::getApiEnvironmentType() === 'Development');
	}
	
	/**
	 * getDevIpAddress
	 * 
	 * @return string
	 */
	public static function getDevIpAddress() {
		return self::$devIpAddress;
	}
	
	/**
	 * getUserIp
	 * 
	 * @return string
	 */
	public static function getUserIp() {
		if (\getenv('HTTP_X_FORWARDED_FOR')) {
			$result = \getenv('HTTP_X_FORWARDED_FOR');
		} else {
			$result = \getenv('REMOTE_ADDR');
		}
		
		return $result;
    }
	
	/**
	 * getApiEnvironmentType
	 * 
	 * @return string
	 */
	protected static function getApiEnvironmentType() {
		return \getenv('API_CONTEXT');
	}
}