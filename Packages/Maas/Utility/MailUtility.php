<?php
namespace Maas\Utility;

/**
 * MailUtility
 */
final class MailUtility {
	
	const EMAIL_EMPFAENGER = 'dev-maas@treaction.de';
	
	/**
	 * emailSubject
	 * 
	 * @var string
	 */
	public static $emailSubject = 'Treaction API Error - api.treaction.de';
	
	/**
	 * headerDataArray
	 * 
	 * @var array
	 */
	static protected $headerDataArray = array(
		'MIME-Version: 1.0',
		'Content-type: text/plain; charset=iso-8859-1',
		'From: Treaction Api Mailer <api@treaction.de>'
	);
	
	
	
	
	
	/**
	 * sendMail
	 * 
	 * @param string $message
	 * @return void
	 */
	public static function sendMail($message) {
		\mail(
			self::EMAIL_EMPFAENGER,
			\getenv('API_CONTEXT') . ' ' . static::$emailSubject,
			$message,
			self::createHeaderData()
		);
	}
	
	/**
	 * sendEmailToUser
	 * 
	 * @param string $userEmail
	 * @param string $message
	 * @return void
	 */
	public static function sendEmailToUser($userEmail, $message) {
		\mail(
			$userEmail,
			static::$EMAIL_BETREFF,
			$message,
			self::createHeaderData()
		);
	}
	
	
	
	/**
	 * createHeaderData
	 * 
	 * @return string
	 */
	protected static function createHeaderData() {
		$headerDataArray = \array_merge(
			self::$headerDataArray,
			array(
				'X-Mailer: PHP/' . \phpversion()
			)
		);
		
		return \implode(\chr(10), $headerDataArray);
	}

}