<?php
namespace Maas\Utility;

/**
 * FileUtility
 */
final class FileUtility {
	
	/**
	 * isDir
	 * 
	 * @param string $filename
	 * @throws \RuntimeException
	 * @return boolean
	 */
	public static function isFile($filename) {
		if (!\is_file($filename)) {
			throw new \RuntimeException('no valid file given: ' . \htmlspecialchars($filename), 1432800216);
		}
		
		return true;
	}
	
	/**
	 * isReadable
	 * 
	 * @param string $filename
	 * @throws \RuntimeException
	 * @return boolean
	 */
	public static function isReadable($filename) {
		self::isFile($filename);
		
		if (!\is_readable($filename)) {
			throw new \RuntimeException('file is not readable: ' . \htmlspecialchars($filename), 1432800257);
		}
		
		return true;
	}
	
	/**
	 * isWritable
	 * 
	 * @param string $filename
	 * @throws \RuntimeException
	 * @return boolean
	 */
	public static function isWritable($filename) {
		self::isFile($filename);
		
		if (!\is_writable($filename)) {
			throw new \RuntimeException('file/dir is not writable: ' . \htmlspecialchars($filename), 1432800671);
		}
		
		return true;
	}
}