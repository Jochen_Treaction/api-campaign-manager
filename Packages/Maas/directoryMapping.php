<?php
$packagesDirectoryMapping = array(
	'Entity' => __DIR__ . \DIRECTORY_SEPARATOR . 'Entity',
	'Factory' => __DIR__ . \DIRECTORY_SEPARATOR . 'Factory',
	'Repository' => __DIR__ . \DIRECTORY_SEPARATOR . 'Repository',
	'Utility' => __DIR__ . \DIRECTORY_SEPARATOR . 'Utility'
);