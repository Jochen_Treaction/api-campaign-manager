<?php
namespace Maas\Factory;

/**
 * DbFactory
 */
abstract class DbFactory {
	static public $fieldEqual = ' = ';
	static public $fieldNotEqual = ' != ';
	static public $fieldIn = ' IN ';
	static public $fieldNotIn = ' NOT IN ';
	static public $fieldGreaterThan = ' > ';
	static public $fieldLessThan = ' < ';
	static public $fieldGreaterOrEqualThan = ' >= ';
	static public $fieldLessOrEqualThan = ' <= ';

	/**
	 * host
	 *
	 * @var string
	 */
	protected $host = null;

	/**
	 * dbName
	 *
	 * @var string
	 */
	protected $dbName = null;

	/**
	 * dbUser
	 *
	 * @var string
	 */
	protected $dbUser = null;

	/**
	 * dbPassword
	 *
	 * @var string
	 */
	protected $dbPassword = null;

	/**
	 * table
	 *
	 * @var string
	 */
	protected $table = null;

	/**
	 * fetchClass
	 *
	 * @var string
	 */
	protected $fetchClass = null;

	/**
	 * @var \PDO
	 */
	protected $dbh = null;





	/**
	 * init
	 *
	 * @return void
	 */
	public function init() {
		try {
			$this->dbh = new \PDO(
				'mysql:host=' . $this->host . ';dbname=' . $this->dbName,
				$this->dbUser,
				$this->dbPassword,
				array(
					\PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'"
				)
			);
			$this->dbh->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
		} catch (\PDOException $e) {
			// TODO: email
			throw new \Exception('connection Error!', 1424432963);
		}
	}



    /**
     * @param  string  $hinweis
     * @param  string|array|object  $message
     * @param  string  $file
     * @param  int|string  $line
     * @param  bool $OVERWRITE
     * @param  bool $logToError
     */
    private function wlog($hinweis, $message, $file, $line, $OVERWRITE = false, $logToError = false)
    {
        // log to path Packages/Maas/Model/Repository
        if (false) { // COMMENT: LOG => TURN ON (TRUE) AN OFF (FALSE) HERE
            $logFile = ($logToError) ? '/errorLog_CampaignRepository.log' : '/log_CampaignRepository.log';
            try {
                $msg = "[" . date('Y-m-d H:i:s') . "] " . $hinweis . ' = ';
                $msg .= print_r($message, true);
                $msg .= "    [$file, $line]\n"; // cron/blCron/blacklistCron_2.php
                if ($OVERWRITE) {
                    file_put_contents(__DIR__ . $logFile, $msg);
                } else {
                    file_put_contents(__DIR__ . $logFile, $msg, FILE_APPEND);
                }
            } catch (Exception $e) {
                file_put_contents(__DIR__ . '/wlogErrorCampaignRepository.log', $e->getMessage(), FILE_APPEND);
            }
        }
    }


	/********************************************************************************************
	 *
	 *              helper - Functions
	 *
	 ****************************************************************************************** */
	/**
	 * createPdoInString
	 *
	 * @param array $dataArray
	 * @return string
	 */
	protected function createPdoInString(array $dataArray) {
		$itemHoldersDataArray = array();
		foreach ($dataArray as $item) {
			$itemHoldersDataArray[] = ':' . $item;
		}

		return \implode(',', $itemHoldersDataArray);
	}

	/**
	 * masketField
	 *
	 * @param string $field
	 * @return string
	 */
	protected function masketField($field) {
		return '`' . \strip_tags($field) . '`';
	}

	/**
	 * processBindWhereParameter
	 *
	 * @param array $queryPartsWhereDataArray
	 * @param \PDOStatement $stmt
	 * @return void
	 */
	protected function processBindWhereParameter(array $queryPartsWhereDataArray, \PDOStatement &$stmt) {
		foreach ($queryPartsWhereDataArray as $key => &$item) {
			if (\strlen($item['value']) > 0) {
				switch ($item['comparison']) {
					case 'LIKE BINARY':
						$string = '%' . $item['value'] . '%';
						$stmt->bindParam(':' . $key, $string, \PDO::PARAM_STR);
						break;

					case 'fieldEqual':
					case 'fieldNotEqual':
						// do nothings
						break;

					case 'integerEqual':
						$stmt->bindParam(':' . $key, $item['value'], \PDO::PARAM_INT);
						break;

					default:
						$stmt->bindParam(':' . $key, $item['value'], \PDO::PARAM_STR);
						break;
				}
			}
		}
	}

	/**
	 * addWhereQry
	 *
	 * @param array $queryPartsWhereDataArray
	 * @return string
	 */
	protected function addWhereQry(array $queryPartsWhereDataArray) {
		$addQuery = '';
		foreach ($queryPartsWhereDataArray as $key => $itemDataArray) {
			if (\strlen($itemDataArray['value']) > 0) {
				switch ($itemDataArray['comparison']) {
					case 'fieldEqual':
						$addQuery .= ' AND ' . $itemDataArray['sql'] . self::$fieldEqual . \trim($itemDataArray['value']);
						break;

					case 'integerEqual':
						$addQuery .= ' AND ' . $itemDataArray['sql'] . self::$fieldEqual . ':' . $key;
						break;

					case 'fieldNotEqual':
						$addQuery .= ' AND ' . $itemDataArray['sql'] . self::$fieldNotEqual . \trim($itemDataArray['value']);
						break;

					default:
						$addQuery .= ' AND ' . $itemDataArray['sql'] . ' ' . $itemDataArray['comparison'] . ' :' . \trim($key);
						break;
				}
			} else {
				if ($itemDataArray['comparison'] == '<>') {
					$addQuery .= ' AND ' . $itemDataArray['sql'] . ' ' . $itemDataArray['comparison'] . ' ""';
				} elseif (\strlen($itemDataArray['comparison']) === 0) {
					$addQuery .= ' AND ' . $itemDataArray['sql'];
				}
			}
		}

		return $addQuery;
	}

	/**
	 * processBindSetParameter
	 *
	 * @param array $dataArray
	 * @param \PDOStatement $stmt
	 * @return void
	 */
	protected function processBindSetParameter(array $dataArray, \PDOStatement &$stmt) {
		foreach ($dataArray as $field => &$item) {
			$stmt->bindParam(':' . $field, $item['value'], $item['dataType']);
		}
	}

	/**
	 * getSelectFieldsQry
	 *
	 * @param array $queryPartsSelectDataArray
	 * @return string
	 */
	protected function getSelectFieldsQry(array $queryPartsSelectDataArray) {
		$selectFields = '';
		foreach ($queryPartsSelectDataArray as $key => $item) {
			$selectFields .= $item . ' as ' . $this->masketField($key) . ', ';
		}

		return \rtrim($selectFields, ', ');
	}

	/**
	 * processDataArrayByQueryParts
	 *
	 * @param array $queryPartsDataArray
	 * @param string $addWhere
	 * @return false|\PDOStatement
	 */
	protected function processDataArrayByQueryParts(array $queryPartsDataArray, $addWhere = '') {
		$selectFields = '*';
		if (isset($queryPartsDataArray['SELECT'])) {
			if (\is_array($queryPartsDataArray['SELECT'])
				&& \count($queryPartsDataArray['SELECT']) > 0
			) {
				$selectFields = $this->getSelectFieldsQry($queryPartsDataArray['SELECT']);
			} else {
				$selectFields = $queryPartsDataArray['SELECT'];
			}
		}

		$addWhereQuery = '';
		if (isset($queryPartsDataArray['WHERE'])
			&& \count($queryPartsDataArray['WHERE']) > 0
		) {
			$addWhereQuery = $this->addWhereQry($queryPartsDataArray['WHERE']);
		}

		$qry = 'SELECT ' . $selectFields
			. ' FROM ' . $this->table
			. ' WHERE 1 = 1'
				. $addWhereQuery
				. $addWhere
			. (isset($queryPartsDataArray['GROUP_BY']) ? ' GROUP BY ' . $queryPartsDataArray['GROUP_BY'] : '')
			. (isset($queryPartsDataArray['ORDER_BY']) ? ' ORDER BY ' . $queryPartsDataArray['ORDER_BY'] : '')
			. (isset($queryPartsDataArray['LIMIT']) ? ' LIMIT ' . $queryPartsDataArray['LIMIT'] : '')
		;

		$this->wlog('$qry', $qry, __FILE__, __LINE__); // REMOVE

		try {
			$stmt = $this->dbh->prepare($qry);

			if (isset($queryPartsDataArray['WHERE'])
				&& \count($queryPartsDataArray['WHERE']) > 0
			) {
				$this->processBindWhereParameter(
					$queryPartsDataArray['WHERE'],
					$stmt
				);
			}

			$stmt->execute();
		} catch (\PDOException $e) {
			// TODO: email
			throw new \Exception('pdo Error!', 1441881299);
		}

		return $stmt;
	}

	/**
	 * countResultByQueryParts
	 *
	 * @param array $queryPartsDataArray
	 * @return type
	 */
	public function countResultByQueryParts(array $queryPartsDataArray) {
		$queryPartsDataArray['SELECT'] = array(
			'count' => 'COUNT(1)'
		);
		unset(
			$queryPartsDataArray['ORDER_BY'],
			$queryPartsDataArray['LIMIT']
		);

		$stmt = $this->processDataArrayByQueryParts(
			$queryPartsDataArray,
			' AND ' . $this->masketField($this->primaryField) . ' > 1'
		);

		return $stmt->fetchColumn();
	}

	/**
	 * fetchStmt
	 *
	 * @param \PDOStatement $stmt
	 * @return mixed
	 * @throws \DomainException
	 */
	protected function fetchStmt(\PDOStatement &$stmt) {
		$stmt->setFetchMode(\PDO::FETCH_CLASS, $this->fetchClass);

		if (($row = $stmt->fetch()) !== false) {
			$stmt->closeCursor();

			return $row;
		} else {
			throw new \DomainException('no ' . $this->fetchClass, 1424433041);
		}
	}

	/**
	 * fetchAllStmt
	 *
	 * @param \PDOStatement $stmt
	 * @return false|array
	 * @throws \DomainException
	 */
	protected function fetchAllStmt(\PDOStatement &$stmt) {
		$stmt->setFetchMode(\PDO::FETCH_CLASS, $this->fetchClass);

		if (($resultDataArray = $stmt->fetchAll()) !== false) {
			$stmt->closeCursor();

			return $resultDataArray;
		} else {
			throw new \DomainException('no ' . $this->fetchClass, 1424433053);
		}
	}





	/********************************************************************************************
	 *
	 *              setter - Functions
	 *
	 ****************************************************************************************** */
	public function setHost($host) {
		$this->host = $host;
	}

	public function setDbName($dbName) {
		$this->dbName = $dbName;
	}

	public function setDbUser($dbUser) {
		$this->dbUser = $dbUser;
	}

	public function setDbPassword($dbPassword) {
		$this->dbPassword = $dbPassword;
	}

	public function setTable($table) {
		$this->table = $table;
	}

	public function setFetchClass($fetchClass) {
		$this->fetchClass = $fetchClass;
	}

}
