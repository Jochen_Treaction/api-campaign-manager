<?php
namespace Maas\Model\Repository;

/**
 * ClientFactory
 */
abstract class AbstractRepository extends \Maas\Factory\DbFactory {
	
	/**
	 * primaryField
	 * 
	 * @var string
	 */
	protected $primaryField = 'id';
	
	
	
	
	
	/**
	 * findAll
	 * 
	 * @return array
	 */
	public function findAll() {
		$stmt = $this->processDataArrayByQueryParts(array());
		
		return $this->fetchAllStmt($stmt);
	}

	/**
	 * findById
	 * 
	 * @param integer $id
	 * @return false|\Maas\Model\Entity\AbstractEntity
	 */
	public function findById($id) {
		$stmt = $this->processDataArrayByQueryParts(
			array(
				'WHERE' => array(
					'id' => array(
						'sql' => $this->masketField($this->primaryField),
						'value' => $id,
						'comparison' => 'integerEqual'
					),
				),
			)
		);
		
		return $this->fetchStmt($stmt);
	}

	/**
	 * findByField
	 * 
	 * @param string $field
	 * @param mixed $fieldValue
	 * @return false|array|\Maas\Model\Entity\AbstractEntity
	 */
	public function findByField($field, $fieldValue) {
		$stmt = $this->processDataArrayByQueryParts(
			array(
				'WHERE' => array(
					'abkz' => array(
						'sql' => $this->masketField($field),
						'value' => $fieldValue,
						'comparison' => \Maas\Factory\DbFactory::$fieldEqual
					),
				),
			)
		);
		
		$result = $this->fetchAllStmt($stmt);
		if (count($result) === 1) {
			\current($result);
		}
		
		return $result;
	}
	
}