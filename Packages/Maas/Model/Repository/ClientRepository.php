<?php
namespace Maas\Model\Repository;

/**
 * ClientFactory
 */
class ClientRepository extends \Maas\Model\Repository\AbstractRepository {
	
	/**
	 * taböe
	 * 
	 * @var string
	 */
	protected $table = 'mandant';
	
	/**
	 * fetchClass
	 * 
	 * @var string
	 */
	protected $fetchClass = '\Maas\Model\Entity\ClientEntity';
}