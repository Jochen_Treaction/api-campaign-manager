<?php
namespace Maas\Model\Repository;

/**
 * CampaignRepository
 */
class CampaignRepository extends \Maas\Model\Repository\AbstractRepository {

	/**
	 * primaryField
	 *
	 * @var string
	 */
	protected $primaryField = 'k_id';

	/**
	 * fetchClass
	 *
	 * @var string
	 */
	protected $fetchClass = '\Maas\Model\Entity\CampaignEntity';


	/**
	 * getCampaignsDataItemsByQueryParts
	 *
	 * @param array $queryPartsDataArray
	 * @param integer $fetchMode
	 * @return false|array
	 */
	public function getCampaignsDataItemsByQueryParts(array $queryPartsDataArray, $fetchMode = \PDO::FETCH_ASSOC) {
		$stmt = $this->processDataArrayByQueryParts(
			$queryPartsDataArray,
			' AND ' . $this->masketField($this->primaryField) . ' > 1'
		);


		return ($fetchMode === \PDO::FETCH_CLASS ? $this->fetchAllStmt($stmt) : $stmt->fetchAll($fetchMode));
	}

	/**
	 * getCampaignDataItemByQueryParts
	 *
	 * @param array $queryPartsDataArray
	 * @param integer $fetchMode
	 * @return false|array|CampaignEntity
	 */
	public function getCampaignDataItemByQueryParts(array $queryPartsDataArray, $fetchMode = \PDO::FETCH_ASSOC) {
		$stmt = $this->processDataArrayByQueryParts(
			$queryPartsDataArray,
			' AND ' . $this->masketField($this->primaryField) . ' > 1'
		);

		return ($fetchMode === \PDO::FETCH_CLASS ? $this->fetchStmt($stmt) : $stmt->fetch($fetchMode));
	}


	/**
	 * updateFetchedObject
	 *
	 * @param \Maas\Model\Entity\AbstractEntity $object
	 * @return void
	 */
	protected function updateFetchedObject(\Maas\Model\Entity\CampaignEntity &$object) {
		$object->setDatum($object->getDatum());
		$object->setGestartet($object->getGestartet());
		$object->setBeendet($object->getBeendet());
		$object->setErstreporting($object->getErstreporting());
		$object->setEndreporting($object->getEndreporting());
		$object->setLast_update($object->getLast_update());
	}

	/**
	 * fetchAllStmt
	 *
	 * @param \PDOStatement $stmt
	 * @return array
	 * @throws \DomainException
	 */
	protected function fetchAllStmt(\PDOStatement &$stmt) {
		$stmt->setFetchMode(\PDO::FETCH_CLASS, $this->fetchClass);

		try {
			$resultDataArray = array();
			while (($row = $stmt->fetch()) !== false) {
				/* @var $row \Maas\Model\Entity\CampaignEntity */

				$this->updateFetchedObject($row);

				$resultDataArray[$row->getK_id()] = $row;
			}
			$stmt->closeCursor();
		} catch (\PDOException $e) {
			throw new \DomainException('no ' . $this->fetchClass, 1424433053);
		}

		return $resultDataArray;
	}


	/**
	 * fetchStmt
	 *
	 * @param \PDOStatement $stmt
	 * @return false|Maas\Model\Entity\CampaignEntity
	 * @throws \DomainException
	 */
	protected function fetchStmt(\PDOStatement &$stmt) {
		$stmt->setFetchMode(\PDO::FETCH_CLASS, $this->fetchClass);

		if (($row = $stmt->fetch()) !== false) {
			$this->updateFetchedObject($row);

			$stmt->closeCursor();

			return $row;
		} else {
			throw new \DomainException('no ' . $this->fetchClass, 1424433041);
		}
	}
}
